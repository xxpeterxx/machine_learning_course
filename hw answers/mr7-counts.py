'''
Question 7: Top100DoubleLinkSimpleWikipedia
Instead of analyzing single links, let's look at double links. That is, pages A and C that are connected through many pages B where there is a link  A -> B -> C Find the list of all pairs (A, C) that have the 100 "most" connections (see below for the definition of "most"). This should give us a notion that the articles A and C refer to tightly related concepts.
This is essentially a Matrix Multiplication problem. If the adjacency matrix is denoted M (where Mij represents the link between i an j), we are looking for the highest 100 elements of the matrix MM.
Notice that a lot of Category pages (denoted "Category:.*") have a high link count and will rank very highly according to this metric. Wikipedia also has Talk: pages, Help: pages, and static resource Files:. All such "non-content" pages (and there might be more than just this) and links to them should be first filtered out in this analysis.
Some pages have more links than others. If we just counted the number of double links between pages, we will end up seeing a list of articles with many links, rather than concepts that are tightly connected.
a. One strategy is to weight each link as 1n where n is the number links on the page. This way, an article has to spread it's "influence" over all n of its links. However, this can throw off the results if n is small.
b. Instead, try weighting each link as 1n+10 where 10 sets the "scale" in terms of number of links above which a page becomes "significant". The number 10 was somewhat arbitrarily chosen but seems to give reasonably relevant results.
Again, if there are multiple links from a page to another, have it only count for 1. This keeps our results from becoming skewed by a single page that references the same page multiple times.
Don't be afraid if these answers are not particularly insightful. Simplewikipedia is not as rich as Englishwikipedia. However, you should notice that the articles are closely related conceptually.
'''

from mrjob.job import MRJob
from mrjob.compat import get_jobconf_value
from mrjob.protocol import JSONValueProtocol
from mrjob.step import MRStep
import re
import heapq
import xml.etree.ElementTree as ET
import mwparserfromhell
from math import sqrt
import random
import itertools
from collections import defaultdict

# WORD_RE = re.compile(r"[\w']+")
WORD_RE = re.compile(r"\w+")
# list_string = []

class MRMostUsedWord(MRJob):

#     OUTPUT_PROTOCOL = JSONValueProtocol

    def mapper_init(self):
        self.list_string = []
        self.list_title = []
        self.list_links = []
        self.node = {}

    def mapper(self, _, line):
        # yield (title, (link, score)) for each page-link combination
        
        self.list_string.append(line)
        if '</page>' in line:
            page_string = "".join(self.list_string)
#             print(self.list_string)
            self.list_string = []
            
            try:
                root = ET.fromstring(page_string)
                
                title = root.find('title').text.lower()
                
                for revision in root.findall('revision'):
                    text = revision.find('text').text
            
                wikicode = mwparserfromhell.parse(text)
                links = wikicode.filter_wikilinks()
                links = [unicode(n).lower()[2:-2] for n in links]
                links = [link.split("|")[0] for link in links if not ('category:' in link or 'talk:' in link or 'help:' in link or 'file:' in link or 'template:' in link)]

                links = list(set(links))
                lenlink = len(links)
                
                if not ('category:' in title or 'talk:' in title or 'help:' in title or 'file:' in title or 'template:' in title):
                    
                    self.list_title.append(title)
                    
                    for link in links:
                        self.list_links.append([title, link, 1./(lenlink + 10)])
                
            except:
                pass
    
    def mapper_final(self):
    
#         self.list_links = sorted(self.list_links, key = lambda x: x[1])
        for element in self.list_links:
            if element[1] in self.list_title:
                yield (element[0], element[1]), element[2]
                # title, (link, score)

    def reducer(self, link, values):
        yield link, sum(values)

    def mapper_matrix_multiply(self, link, value):
        
        for times in range(2):
            if times == 0:
                i = link[0]
                j = link[1]
                v = value
                yield j, (0, i, v)
            
            if times == 1:
                j = link[0]
                k = link[1]
                v = value
                yield j, (1, k, v)

    def multiply_values(self, j, values):

        values_from1 = []
        values_from2 = []
        
        for v in values:
            if v[0] == 0:
                values_from1.append(v)
            elif v[0] == 1:
                values_from2.append(v)
        
        for (m, i, v1) in values_from1:
            for (m, k, v2) in values_from2:
                yield (i, k), v1 * v2
        
    def identity(self, k, v):
        yield k, v
    
    def add_values(self, k, values):
        yield k, sum(values)
        
    def steps(self):
        return [
            MRStep(mapper_init = self.mapper_init,
                   mapper = self.mapper,
                   mapper_final = self.mapper_final,                 
                   reducer = self.reducer),
            MRStep(mapper = self.mapper_matrix_multiply,
                   reducer = self.multiply_values),
            MRStep(mapper = self.identity,
                   reducer = self.add_values)
        ]


if __name__ == '__main__':
    MRMostUsedWord.run()
