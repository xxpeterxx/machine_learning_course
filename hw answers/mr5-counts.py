'''
Notice that the words page and text make it into the top 100 words in the prevous problem. These are not common English words! If you look at the xml formatting, you'll realize that these are xml tags. You should parse the files so that tags like <page></page> should not be included in your total, nor should words outside of the tag <text></text>. 

Hints:
1. Notice that xml will parse a string as xml and is part of the standard library.
2. In order to parse the text, we will have to accumulate a <page></page> worth of data and then parse the resulting wikipedia format string.
'''

from mrjob.job import MRJob
from mrjob.compat import get_jobconf_value
from mrjob.protocol import JSONValueProtocol
from mrjob.step import MRStep
import re
import heapq
import xml.etree.ElementTree as ET
import mwparserfromhell
from math import sqrt
import random


# WORD_RE = re.compile(r"[\w']+")
WORD_RE = re.compile(r"\w+")
# list_string = []

class MRMostUsedWord(MRJob):

    OUTPUT_PROTOCOL = JSONValueProtocol

    def mapper_init(self):
        self.list_string = []

    def mapper_get_counts(self, _, line):
        # yield each word in the line

        self.list_string.append(line)
        if '</page>' in line:
            page_string = "".join(self.list_string)
#             print(self.list_string)
            self.list_string = []
            
            try:
                root = ET.fromstring(page_string)
                
                for revision in root.findall('revision'):
                    text = revision.find('text').text
            
                wikicode = mwparserfromhell.parse(text)
                links = wikicode.filter_wikilinks()
                links = [unicode(n) for n in links]
                lenlink = len(list(set(links)))
                
                var = [lenlink, lenlink*lenlink]
                
                yield 1, var
                
            except:
                pass
                

    def reducer(self, n, vars):
        
        N_res = 10000
        
        N = 0.0
        sum = 0.0
        sumsq = 0.0
        list_n = []
        
        for x in vars:
            N += 1
            sum += x[0]
            sumsq += x[1]
            
            if N <= N_res:
                list_n.append(x[0])
            else:
                random_n = random.random()
                if random_n < N_res/N:
                    random.shuffle(list_n)
                    list_n.pop()
                    list_n.append(x[0])
                else:
                    pass
        
        list_n = sorted(list_n)
        
        q5, q25, q50, q75, q95 = list_n[500], list_n[2500], list_n[5000], list_n[7500], list_n[9500]
        
        mean = sum/N
        
        sd = sqrt(sumsq/N - mean*mean)
        
        
        results = [N, mean, sd, q5, q25, q50, q75, q95]
        
        yield 1, results


    def steps(self):
        return [
            MRStep(mapper_init = self.mapper_init,
                   mapper = self.mapper_get_counts,
                   reducer = self.reducer)
        ]


if __name__ == '__main__':
    MRMostUsedWord.run()
