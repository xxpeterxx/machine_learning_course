from mrjob.job import MRJob
from mrjob.compat import get_jobconf_value
from mrjob.protocol import JSONValueProtocol
from mrjob.step import MRStep
import re
import heapq


# WORD_RE = re.compile(r"[\w']+")
WORD_RE = re.compile(r"\w+")


class MRMostUsedWord(MRJob):

    OUTPUT_PROTOCOL = JSONValueProtocol

    def mapper_get_words(self, _, line):
        # yield each word in the line
        for word in WORD_RE.findall(line):
            yield (word.lower(), 1)

    def combiner_count_words(self, word, counts):
        # optimization: sum the words we've seen so far
        yield (word, sum(counts))

    def reducer_count_words(self, word, counts):
        # send all (num_occurrences, word) pairs to the same reducer.
        # num_occurrences is so we can easily use Python's max() function.
        yield None, (word, sum(counts))
    
    def reducer_find_max_n_words(self, _, word_count_pairs):
        # each item of word_count_pairs is (word, count),
        # so yielding one results in key=word, value=count
#         yield max(word_count_pairs)
        h = []
        for word_pair in word_count_pairs:
            heapq.heappush(h, word_pair)
            
        yield None, heapq.nlargest(100, h, key = lambda x:x[1])
        
    def steps(self):
        return [
            MRStep(mapper = self.mapper_get_words,
                   combiner = self.combiner_count_words,
                   reducer = self.reducer_count_words),
            MRStep(reducer = self.reducer_find_max_n_words)
        ]


if __name__ == '__main__':
    MRMostUsedWord.run()
