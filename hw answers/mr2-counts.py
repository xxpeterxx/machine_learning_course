'''
Notice that the words page and text make it into the top 100 words in the prevous problem. These are not common English words! If you look at the xml formatting, you'll realize that these are xml tags. You should parse the files so that tags like <page></page> should not be included in your total, nor should words outside of the tag <text></text>. 

Hints:
1. Notice that xml will parse a string as xml and is part of the standard library.
2. In order to parse the text, we will have to accumulate a <page></page> worth of data and then parse the resulting wikipedia format string.
'''

from mrjob.job import MRJob
from mrjob.compat import get_jobconf_value
from mrjob.protocol import JSONValueProtocol
from mrjob.step import MRStep
import re
import heapq
import xml.etree.ElementTree as ET

# WORD_RE = re.compile(r"[\w']+")
WORD_RE = re.compile(r"\w+")
# list_string = []

class MRMostUsedWord(MRJob):

    OUTPUT_PROTOCOL = JSONValueProtocol

    def mapper_init(self):
        self.list_string = []

    def mapper_get_words(self, _, line):
        # yield each word in the line

        self.list_string.append(line)
        if '</page>' in line:
            page_string = "".join(self.list_string)
#             print(self.list_string)
            self.list_string = []
            
            try:
                root = ET.fromstring(page_string)
                
                for revision in root.findall('revision'):
                    text = revision.find('text').text
            
                for word in WORD_RE.findall(text):
                    yield (word.lower(), 1)
                    
            except:
                pass
                
    def combiner_count_words(self, word, counts):
        # optimization: sum the words we've seen so far
        yield (word, sum(counts))

    def reducer_count_words(self, word, counts):
        # send all (num_occurrences, word) pairs to the same reducer.
        # num_occurrences is so we can easily use Python's max() function.
        yield None, (word, sum(counts))
        
#     def mapper_find_max_n_words(self, _, word_count_pairs):
#         h = []
#         for word_pair in word_count_pairs:
#             heapq.heappush(h, word_pair)
#             if len(h) > 100:
#                 heapq.heappop(h)
#                 
#         yield None, heapq.nlargest(100, h, key = lambda x:x[1])
#     
    def reducer_find_max_n_words(self, _, word_count_pairs):
#         yield max(word_count_pairs)
        h = []
        for word_pair in word_count_pairs:
            heapq.heappush(h, word_pair)
#             if len(h) > 100:
#                 heapq.heappop(h)
            
        yield None, heapq.nlargest(100, h, key = lambda x:x[1])
        
    def steps(self):
        return [
            MRStep(mapper_init = self.mapper_init,
                   mapper = self.mapper_get_words,
                   combiner = self.combiner_count_words,
                   reducer = self.reducer_count_words),
#             MRStep(mapper = self.mapper_find_max_n_words, 
            MRStep(reducer = self.reducer_find_max_n_words)
        ]


if __name__ == '__main__':
    MRMostUsedWord.run()
