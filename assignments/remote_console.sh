#!/bin/bash

foreman start -e config/development,config/shared &
PROC=$!
sleep 2
python -i lib/init_console.py

pkill -P $PROC
sleep 2
echo "Done."
