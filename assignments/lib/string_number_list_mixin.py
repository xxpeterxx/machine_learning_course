from .decorator import catch_validate_exception
from numbers import Number
from tuple_list_mixin import TupleListValidateMixin, TupleListScoreMixin

class StringNumberListValidateMixin(TupleListValidateMixin):
  @classmethod
  def list_length(cls):
    return 100

  @classmethod
  def tuple_validators(cls):
    return (cls.validate_string, cls.validate_number)


class StringNumberListScoreMixin(TupleListScoreMixin):
  @classmethod
  def tuple_scorers(cls):
    return (
      cls.approximate_number_scorer(.1),
    )


class StringApproxNumberListScoreMixin(TupleListScoreMixin):
  @classmethod
  def tuple_scorers(cls):
    return (
      cls.approximate_number_scorer(1.),
    )