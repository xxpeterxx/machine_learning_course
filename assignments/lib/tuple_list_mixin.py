from .decorator import catch_validate_exception
from numbers import Number
import difflib
import math

"""
A tuple list is a list of tuples like
[('a', 'b', 1)] * 10
or 
[(('a', 'b'), 1)] * 10

The first element of each tuple must be either a string or tuple and constitues the "key".
The rest of the elements constitue the "values".
"""

class TupleListValidateMixin(object):
  """
  Checks the list is of length `list_length` and each tuple is a list that can be validated by `tuple_length`
  """
  @classmethod
  def list_length(cls):
    raise NotImplementedError

  @classmethod
  def tuple_validators(cls):
    raise NotImplementedError

  @classmethod
  def tuple_length(cls):
    return len(cls.tuple_validators())

  @classmethod
  def validate_string(cls, x):
    if isinstance(x, basestring):
      return None
    return "Expected a string, got %.200s" % str(x)

  @classmethod
  def validate_int(cls, x):
    if isinstance(x, int):
      return None
    return "Expected an int, got %.200s" % str(x)

  @classmethod
  def validate_number(cls, x):
    if isinstance(x, Number):
      return None
    return "Expected a number, got %.200s" % str(x)

  @classmethod
  def validate_float(cls, x):
    if isinstance(x, float):
      return None
    return "Expected a float, got %.200s" % str(x)

  @classmethod
  def validate_tuple(cls, x):
    if isinstance(x, tuple):
      return None
    return "Expected a tuple, got %.200s" % str(x)

  @catch_validate_exception
  def validate(self):
    lst = self.solution()
    if not isinstance(lst, (tuple, list)):
      return "Expected list, got %.200s." % str(lst)

    if len(lst) != self.list_length():
      return "Expected list of length %d, got length %d list %.200s." % (self.list_length(), len(lst), str(lst))

    for row in lst:
      if not isinstance(row, (tuple, list)):
        return "Expected every element to be a tuple, got %.200s." % str(row)

      if len(row) != self.tuple_length():
        return "Expected every element to be of length %d, got %.200s." % (self.tuple_length(), str(row))

      for e, validator in zip(row, self.tuple_validators()):
        result = validator(e)
        if result is not None:
          return result

    return None


class TupleListScoreMixin(object):
  """
  Grades `fellow_answer` by using difflib to find aproximate matches in the `correct` solution
  (Approximately) matches on key and checks the corresponding "values" using `tuple_scorers`
  """
  @classmethod
  def _normalize_key(cls, key):
    if isinstance(key, basestring):
      return key.lower()

    if isinstance(key, (tuple, list)):
      return str(tuple(sorted([name.lower() for name in key])))

    raise ValueError("Expected either str or tuple")

  @classmethod
  def tuple_scorers(cls):
    raise NotImplementedError

  @classmethod
  def approximate_number_scorer(cls, tolerance):
    def _approximate_number_scorer(x, y):
      return math.fabs(1. * (x - y) / ((x + y) / 2.)) < tolerance

    return _approximate_number_scorer

  @classmethod
  def _scorer_tuple(cls, correct_tuple, fellow_dict):
    correct_key = cls._normalize_key(correct_tuple[0])
    match_keys = difflib.get_close_matches(correct_key, fellow_dict.keys(), n=1)
    if len(match_keys) < 1:
      return 0.

    match_value = fellow_dict[match_keys[0]]
    correct_value = correct_tuple[1:]
    for f, m, scorer in zip(correct_value, match_value, cls.tuple_scorers()):
      if not scorer(f, m):
        return 0.
    return 1.

  def score(self, fellow_answers):
    correct_answers = [(self.solution(*case['args'], **case['kwargs']))
      for case in self.get_test_cases()]

    fellow_answer = fellow_answers[0]
    correct_answer = correct_answers[0]

    denominator = max(len(correct_answer), len(fellow_answer))
    fellow_dict = {
      self._normalize_key(fellow_tuple[0]): fellow_tuple[1:]
      for fellow_tuple in fellow_answer
    }

    return 1. * sum([self._scorer_tuple(correct_tuple, fellow_dict) for correct_tuple in correct_answer]) / denominator

