import jsonrpclib

server = jsonrpclib.Server("http://localhost:6631")

functions = server.get_submitted_functions()

print "=============="
print "Welcome!"
print "This mimics the environment we use to get your solutions from your submission."
print "Look below for a list of solutions you can call. Make sure to add the appropriate arguments (as always in python, 'self' is *not* an argument you provide)."
print "=============="
for function in functions:
  print "server.{function}.solution()".format(function=function)

print "=============="
