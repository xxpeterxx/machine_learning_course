from .decorator import catch_validate_exception
from numbers import Number
import math

class JsonValidateMixin(object):
  @classmethod
  def keys(cls):
    raise NotImplementedError

  @catch_validate_exception
  def validate(self):
    ans = self.solution()

    if not isinstance(ans, dict):
      return "Expected dict but got %.100s" % str(ans)

    if set(ans.keys()) != set(self.keys()):
      return "Expected keys %s but got %.100s" % (self.keys(), str(ans))

    return None


class JsonGradeMixin(object):
  @classmethod
  def fraction_error(cls, x, y):
    return math.fabs((x - y) / ((x + y) / 2))

  def score(self, fellow_answers):
    corrects = [(self.solution(*case['args'], **case['kwargs']))
      for case in self.get_test_cases()]

    fellow_answer = fellow_answers[0]
    correct = corrects[0]

    # asssume they have common keys because this is validated
    return 1. * sum([(self.fraction_error(fellow_answer[k], v) < .1)
                          for k, v in correct.iteritems()]) / len(correct)


class YelpJsonRecordMixin(object):
  @classmethod
  def fields(cls):
    raise NotImplementedError

  @classmethod
  def _test_json(cls):
    raise NotImplementedError

  @classmethod
  def get_test_cases(cls):
    return [{ 'args': [cls.get_records()], 'kwargs': {} }]

  @classmethod
  def get_records(cls):
    if cls.fields():
      return [
        { k: tj.get(k, None) for k in cls.fields() }
        for tj in cls._test_json()
      ]
    else:
      return [
        { k: tj.get(k, None) for k in tj.keys() if k != 'stars' }
        for tj in cls._test_json()
      ]

  @classmethod
  def get_ground_truth(cls):
    return [record['stars'] for record in cls._test_json()]


class ListOrDictValidateMixin(YelpJsonRecordMixin):
  @classmethod
  def fields(cls):
    raise NotImplementedError

  @classmethod
  def _test_json(cls):
    raise NotImplementedError

  @classmethod
  def _validate(cls, ans):
    if not isinstance(ans, Number):
      return "Expected number but got %.100s" % str(ans)

    return None

  @catch_validate_exception
  def validate(self):
    # check that this
    for case in self.get_test_cases():
      solutions = self.solution(*case['args'], **case['kwargs'])
      for sol in solutions:
        val = self._validate(sol)

        if val is not None:
          return val

    return None