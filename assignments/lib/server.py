from jsonrpclib.SimpleJSONRPCServer import SimpleJSONRPCServer
import argparse
import os
import sys
import inspect

from lib.base import QuestionList, Answer
from collections import OrderedDict
from lib import validate_answers

def run_server():

  port = int(os.environ.get("PORT", 5000))
  print >>sys.stderr, "JSONRPC server is running on port {port}".format(port=port)

  server = SimpleJSONRPCServer(('0.0.0.0', port))
  server.register_function(get_submitted_functions, 'get_submitted_functions')
  server.register_function(validate_answers.main, 'validate_answers')

  for mod_name, mod_question_list in QuestionList.list().iteritems():
    for question in mod_question_list:
      instantiated_question = question()
      name = mod_name + "." + question.__name__
      if issubclass(question, Answer):
        server.register_function(instantiated_question.score, '{name}.score'.format(name=name))
        server.register_function(instantiated_question.solution, '{name}.solution'.format(name=name))
      else:
        server.register_function(instantiated_question.solution, '{name}.solution'.format(name=name))
        if hasattr(instantiated_question, 'validate'):
          server.register_function(instantiated_question.validate, '{name}.validate'.format(name=name))
        else:
          print >>sys.stderr, "WARNING: question {f} has no validation".format(f=name)

  server.serve_forever()

def get_submitted_functions(question_name=None, solution=False):
  return dict([
    (mod_name + "." + question.__name__,
      {'cases': question().get_test_cases() }
        if solution
        else "None"
    ) for mod_name, question_list in QuestionList.list().iteritems() for question in question_list
    if question_name is None or question.__name__ == question_name
  ])


def main():
  server_mode = os.environ.get("SERV_MODE", "QUESTION")


  # we need to read in the code to add it to QL
  # note: paths set by environment variables
  if server_mode == "SOLUTION":
    import solutions #pylint:disable=F0401,W0611, W0612
  else:
    import questions #pylint:disable=F0401,W0611, W0612

  run_server()

if __name__ == "__main__":
  main()
