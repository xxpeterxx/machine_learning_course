from .decorator import catch_validate_exception
from numbers import Number

class ListValidateMixin(object):
  @catch_validate_exception
  def validate(self):
    lst = self.solution()
    if not isinstance(lst, (tuple, list)):
      return "Expected list, got %.200s." % str(lst)

    if not len(lst) == 100:
      return "Expected list of length 100, got %.200s." % str(lst)

    for e in lst:
      if not isinstance(e, basestring):
        return "Expected every element to be a string, got %.200s." % str(e)

    return None


class NumberValidateMixin(object):
  @classmethod
  def _validate(cls, ans):
    if not isinstance(ans, Number):
      return "Expected number but got %.100s" % str(ans)

    return None

  @catch_validate_exception
  def validate(self):
    # check that this
    for json in sample_json:
      sol = self.solution(json)
      val = self._validate(sol)
      if val is not None:
        return val

    return None


