import traceback

def list_or_dict(f):
  """
  Wraps a function that takes a single dict arguement
  so that it can also take a list of dictionaries
  """
  def wrapped_f(self, d):
    if type(d) is list:
      return [f(self, d_) for d_ in d]
    elif type(d) is dict:
      return f(self, d)
    else:
      raise ValueError("argument should be either a list or dictionary")
  return wrapped_f


def catch_validate_exception(validate):
  def wrapped_validate(self):
    try:
      return validate(self)
    except Exception as e: #pylint:disable=W0703
      return "Your code does not run! Here's a traceback. {tb}".format(tb=traceback.format_exc())
    return None
  return wrapped_validate
