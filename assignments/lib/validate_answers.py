from lib import QuestionList


def main():
  instantiated_questions = [question() for mod_name, question_list in QuestionList.list().iteritems() for question in question_list]
  invalid_question_names = []
  for question in instantiated_questions:
    q_name=question.__class__.__name__
    validation = question.validate()
    if validation is not None:
      print "==== {q_name} ===".format(q_name=q_name)
      print "Error!"
      print validation
      invalid_question_names.append(q_name)

  if len(invalid_question_names) == 0:
    print "All questions successfully validated!"

  return invalid_question_names


if __name__ == "__main__":
  #questions need to be loaded manually
  import questions
  main()
