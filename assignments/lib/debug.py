import sys

class DebugPrint(object):
  def __enter__(self):
    print "#" * 40

  def __exit__(self ,type, value, traceback):
    print "#" * 40
    sys.stdout.flush()
