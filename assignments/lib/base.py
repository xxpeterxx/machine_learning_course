class QuestionList(object):
  """
  Use a decorator to keep track of all the questions
  To avoid introspection
  Singleton object
  """
  _LIST = {}
  _mod_name = ""

  @classmethod
  def add(cls, question):
    if cls._mod_name not in cls._LIST:
      cls._LIST[cls._mod_name] = []
    cls._LIST[cls._mod_name].append(question)
    return question

  @classmethod
  def set_name(cls, name):
    cls._mod_name = name

  @classmethod
  def list(cls):
    return cls._LIST


class Question(object):
  """
  Questions need 2 functions:
  1) solution
  2) validate
  """
  def validate(self):
    raise NotImplementedError

  def solution(self):
    raise NotImplementedError


class Answer(Question):
  """
  Answers need 3 functions:
  1) get_test_cases
  get_test_cases returns either a dict with two values:
    1) args: a list of arguments
    2) kwargs: a list of dictionaries for each keyword argument
  or a list thereof
  2) score
  3) solution (argument structure supplied by get_test_cases)
  """
  def get_test_cases(self):
    return [{'args': [], 'kwargs': {}}]

  def score(self, fellow_answers):
    raise NotImplementedError

  def solution(self):
    raise NotImplementedError

