#!/bin/bash

echo "Validating locally..."
echo "======"
PYTHONPATH="." python -m lib.validate_answers
echo "======"
echo "Done. See above for possible errors."
