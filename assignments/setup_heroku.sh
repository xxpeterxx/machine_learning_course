echo "Setting up local environment..."
ln -s config/development .env
echo "Done."
echo
#On deploy, heroku sends a request to the dataincubator
#grading server notifying of submission
heroku addons:add deployhooks:http \
      --url=http://dataincubator-grading.herokuapp.com/assignmentSubmit

echo "Setting server config variables..."
heroku plugins:install git://github.com/ddollar/heroku-config.git
cat config/production | heroku config:push
cat config/shared | heroku config:push
echo "Set."
echo
echo "OK."
echo "Time for your first submission: run 'git push heroku master'"
