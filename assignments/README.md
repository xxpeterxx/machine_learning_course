Assignments
==========
Hello and welcome to The Data Incubator!

This repository contains the assignments you will need to complete for credit in the course. This README contains

* [Setup instructions](#installation)
* [Assignment 1: Tutorial and Sanity check](#first-assignment)


# Installation

0. Clone this repository (but that's already taken care of).
1. [Sign up for Heroku.](https://heroku.com) (you should have done this already).
2. From the command line, run `heroku login`, and sign in with the credentials you just created.
3. Run the command `heroku create {my-app-name}`, where `my-app-name` can be anything you like.
4. Now run the `setup_heroku.sh` script.
5. As mentioned in the script output, run `git push heroku master`. This may take a minute.
6. Great, you're ready to go!

# First Assignment

This first assignment functions as a sanity check, to ensure you understand the basic flow of completing these assignments. Once you've completed this, you'll be ready to try your hand at the more substantive assignments. (Though you'll probably want to go through some of the lessons, first!)

1. Open the file `questions/ass1.py`. All skeleton code lives in the `questions` directory, and all code you write should live there too.
2. You'll see a class, APlusB. Each class represents a question. Moreover, each question-class has a function called `solution(self, a, b)` returning a stub value.
3. Every question is a class in one of these files. The question descriptions themselves also live in the files as comments. Once you finish assignment 1, you should poke around the files to see what exercises you'll be completing. 
4. Each question *must* implement the solution() function, and that function must return the requested solution in the correct format. Furthermore, do not modify the function signature.
5. The `validate_answers.sh` script can be run to ensure that each solution() function takes the correct arguments and returns a correctly formatted (but not necessarily correct) answer.
6. Hence, in the question APlusB, solution() must return the result of adding together the parameters 'a' and 'b'. Go ahead and implement that.
7. Now run `validate_answers.sh` to ensure you return the desired sort of output (in this case, a single number).
8. Finally, `git add` and `git commit` the solution, then run `git push heroku master` to submit your answer. About a minute after deployment finishes, you can log in at http://www.thedataincubator.com/gradebook.html to see your latest score. You should see a 1.0 next to 'ass1 - APlusB', provided that you correctly implemented the solution.

That's it! Good luck with the course!

# Submission process
So to generalize from above, this is how you'll complete assignments:

1. Save assignment
2. Run `validate_answers.sh` to ensure your code will run upon submission.
3. Add your new code to repo `git add questions/<filename>`
4. `git commit`
5. `git push heroku master`
6. After a few minutes, visit your gradebook. You should see a new submission. 

You can submit as many times as you like. We only look at your latest submission and the relevant questions that are due at that time.

# Installing new python packages
If you wish to use python packages outside of the default installed ones, please make sure that you update `~/assignments/requirements.txt` with the package name and version that you're using. If you open up the file, you'll see the syntax is pretty self-explanatory for doing this.

# General Instructions and things to know

- Do not touch the 'validate' method in any question file, or the solution() method signature.
- You can add arbitrary methods, classes, and files to your python code in the 'questions' directory. As long as each predefined question class's 'solution' method runs correctly, any valid python is fair game.
- Debugging tip: from inside of the `~/assignments` directory, you can run `heroku logs` to get a log of what's happened most recently on your heroku submission. This can help you catch simple errors, such as when you forget to update `requirements.txt` (as explained above)

#Workflow 

- In terms of workflow, you will have the best luck editing the file in one window, and running the code in an interpreter - using reload() to update the solution whenver you edit the files. We will further explain on day 1.
- Of course, you should do whatever works best for you!
