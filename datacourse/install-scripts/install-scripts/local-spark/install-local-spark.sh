#!/usr/bin/bash
set -e
set -x

SCALA_VER=2.11.0
SPARK_VER=1.0.0
SPARK_PLATFORM=hadoop2
LOCAL=${HOME}/local-spark

#sudo apt-get install java-common default-jre default-jdk

mkdir -p ${LOCAL}
cd ${LOCAL}

wget http://downloads.typesafe.com/scala/${SCALA_VER}/scala-${SCALA_VER}.tgz
tar xzf scala-${SCALA_VER}.tgz
export PATH="$LOCAL/scala-${SCALA_VER}/bin/:$PATH"

wget http://d3kbcqa49mib13.cloudfront.net/spark-${SPARK_VER}-bin-${SPARK_PLATFORM}.tgz
tar xzf spark-${SPARK_VER}-bin-hadoop2.tgz
#(cd spark-${SPARK_VER}-bin-${SPARK_PLATFORM} && ./sbt/sbt assembly && ./sbt/sbt test)

SPARK_HOME="${LOCAL}/spark-${SPARK_VER}-bin-${SPARK_PLATFORM}/"

BASHRC="${HOME}/.bashrc"
cat <<EOF >> ${BASHRC}
# Added by Toly's spark installer.
if [ -d "${SPARK_HOME}" ]; then
    export PYTHONPATH="${SPARK_HOME}/python/:\${PYTHONPATH};"
    export PYTHONPATH="${SPARK_HOME}/python/lib/py4j-0.8.1-src.zip:\${PYTHONPATH}";
fi
EOF
