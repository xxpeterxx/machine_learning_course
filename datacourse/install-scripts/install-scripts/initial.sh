#!/bin/bash
set -e

## Create user - not very well automated
NAME="$1"
FULL_NAME="$2"
KEY="$3"

# Make the user
adduser --ingroup users --disabled-password --gecos "${FULL_NAME}" $NAME

# Setting up SSH keys -- system wide, and for the user account
echo "$KEY" >> /root/.ssh/authorized_keys

mkdir /home/$NAME/.ssh
echo "$KEY" >> /home/$NAME/.ssh/authorized_keys
chown -R $NAME.users /home/$NAME/.ssh

# The sudo group must already exist...?
addgroup $NAME sudo

# And update ssh 
echo "ChallengeResponseAuthentication no" >> /etc/ssh/sshd_config 
echo "UsePAM no" >> /etc/ssh/sshd_config 
echo "PasswordAuthentication no" >> /etc/ssh/sshd_config

# Relocate the install files (assumed there!)
chown -R $NAME.users /root/install-scripts/
mv /root/install-scripts /home/$NAME/
