#Manual Install directions
This is also essentially what the automated install scripts do.

First login as root and somehow ensure that this directory (install-scripts/) exists.

On initial login as root:

	$ bash initial.sh "username" "Full Name" "sshkey"

        $ sudo passwd "username"

This will set up the user account `username`, move the install-scripts/ folder to the new user's directory, and (in the last line) set the user's password (for e.g. `sudo`).  

At this point you can login as `username` using the provided ssh key:

	$ bash install-scripts/system-packages.sh

Enter your password when sudo asks, and take a short walk.  This `apt-get install`'s many packages.

	## Python
	$ bash install-scripts/local-python/install-local-python.sh

Go get some coffee as it runs.  This installs a local python install into a virtual env under ~/local-python/, which is configured to be auto-used by the user.


	$ bash install-scripts/local-python/setup-ipython-server.sh

This creates a self-signed SSL key and sets a password for the ipython notebook server.

        ## Spark
        $ bash install-scripts/local-spark/install-local-spark.sh

This installs spark into ~/local-spark/.
	
	##Hadoop
	$ bash datacourse/install-scripts/hadoop.sh

Enter your password when sudo asks, then go get more coffee as Hadoop installs.
