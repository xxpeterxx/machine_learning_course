#!/bin/bash
set -e  ## Terminate on non-zero exit code.

##

PROFILE=nbserver
PORT=8889
PROFILE_DIR=${HOME}/.ipython/profile_${PROFILE}
CERT_PATH=${PROFILE_DIR}/${PROFILE}.pem

#####################

MY_DIR=`dirname $0`
. ${MY_DIR}/utilities.sh

echo_green "Creating iPython profile."
ipython profile create ${PROFILE}

echo
echo_green "Creating self-signed SSL certification:"
openssl req -x509 -nodes -days 365 -newkey rsa:1024 -keyout ${CERT_PATH} -out ${CERT_PATH}

echo
echo_green "Pick a password to use when connecting:"
PASS=$(/bin/echo -e "import IPython.lib\np=IPython.lib.passwd()\nprint p"|python | tail -1)

##Generate a cookie secret
SECRET=$(python -c 'import uuid,base64; print base64.b64encode(uuid.uuid1().bytes)')
echo
echo_green "Updating configuration file:"
cat << EOF >> ${PROFILE_DIR}/ipython_notebook_config.py
c = get_config()
c.IPKernelApp.matplotlib = 'inline'
c.NotebookApp.certfile = u'${CERT_PATH}'
c.NotebookApp.open_browser = False
c.NotebookApp.password = u'${PASS}'
c.NotebookApp.ip = '*'
c.NotebookApp.port = ${PORT}
c.NotebookApp.cookie_secret = '${SECRET}'
EOF
echo "Done."


if [ ! -d ${HOME}/.ipython/profile_old ]; then
    mv ${HOME}/.ipython/profile_default ${HOME}/.ipython/profile_old
    OLD_NAME="old"
else
    OLD_TARGET=$(mktemp -d ${HOME}/.ipython/profile_old.XXXXX)
    rmdir ${OLD_TARGET}
    mv ${HOME}/.ipython/profile_default ${OLD_TARGET}
    OLD_NAME=$(echo ${OLD_TARGET}|rev|cut -d"/" -f1|rev|cut -d"_" -f2-)
fi

echo
echo_green "Making nbserver the default ipython profile. (The old default is renamed to '${OLD_NAME}'.  Change the symlink in ~/.ipython to undo.)"
ln -s ${PROFILE_DIR} ${HOME}/.ipython/profile_default

echo
echo_green "All set!"
echo
echo "To launch your notebook, type:"
echo_blue "   ipython notebook "
echo "(You may first have to launch a new shell if you just installed python.)"
echo "Connect to it at"
MY_IP=$(curl -s ifconfig.me/ip)
echo_blue "   https://${MY_IP}:${PORT}"
echo "and ignore the safety warning."
