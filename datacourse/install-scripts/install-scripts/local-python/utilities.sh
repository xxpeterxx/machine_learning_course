#!/bin/bash

ECHO="echo"
echo_red(){ $ECHO -e "\033[31m$1\033[0m"; }
echo_green(){ $ECHO -e "\033[32m$1\033[0m"; }
echo_blue(){ $ECHO -e "\033[34m$1\033[0m"; }


function is_mac() {
    [ `uname` == "Darwin" ]
}

function is_mavericks() {
    [ `uname -r|cut -d"." -f1` == "13" ]
}

has_homebrew() {
    command -v brew >/dev/null 2>&1
}

check_pkg_and_brew() {
    PKG=$1
    BREW=$2
    if pkg-config --exists ${PKG} ; then return 0; fi
    if ! modify_global_is_set && is_mac && has_homebrew ; then
        echo_green "Installing ${BREW} using Homebrew."
        brew install --universal ${BREW} 
        return 0
    fi
    return 1

}


