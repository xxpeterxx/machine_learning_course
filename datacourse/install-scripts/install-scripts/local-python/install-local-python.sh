#/bin/bash
set -e  ## Terminate on non-zero exit code.

##

export PYTHON_VER=2.7.6
export VIRTUALENV_VER=1.11.5
export PYTHON_PATH=${HOME}/local-python/${PYTHON_VER}/
export INITIAL_ENV=py-init-env
export MAKEFLAGS="-j4"

#####################

MY_DIR=`dirname $0`
. ${MY_DIR}/utilities.sh

## Utilities and checking for packages
detect_curl() {
    # Don't mess with it if it's already set, otherwise try to autodetect
    if [ -n "${CURL}" ]; then
        return 0
    fi
    if command -v curl >/dev/null 2>&1; then
        export CURL="curl -L "
        return 0
    elif  command -v wget >/dev/null 2>&1; then
        export CURL="wget -O - "
        return 0
    fi
    return 1
}

check_zeromq() {
    check_pkg_and_brew libzmq zeromq
}

check_freetype() {
    check_pkg_and_brew freetype2 freetype
}

check_libpng() {
    check_pkg_and_brew libpng libpng
}

check_gfortran() 
{
    if command -v gfortran >/dev/null 2>&1 ; then
        return 0
    fi
    if ! modify_global_is_set; then
        return 1
    fi
    if is_mac && has_homebrew; then 
        if command -v brew >/dev/null 2>&1 ; then
            echo_green "Installing gfortran from Homebrew."
            brew install gfortran
            return 0
        else
            echo_red "Homebrew not found -- wanted to use it to install gfortran."
        fi
    fi
    return 1
}


## Build steps
build_in_progress() {
    [ -d ${PYTHON_PATH} ] && [ -d build-${PYTHON_VER} ]
}


# Install Python from brew.
# I'd prefer to do it more locally, but this lets us have
# a "Framework" copy.
install_python_osx() {
    PYTHON_FILE=Python-brew
    if [ -f ${PYTHON_FILE}.built ]; then 
        export PYTHON_SYS="/usr/local/bin/python"
        return 0
    fi

    echo_green 
    echo_green "Installing Python from Homebrew."
    echo_green

    if ! command -v brew >/dev/null 2>&1; then
        echo_red "Error: Homebrew not installed.  Was going to install homebrew's Python!"
        return 1
    fi

    if ! brew install python ; then 
        echo_red "Error: Couldn't brew install python!";
        return 1 
    else
        brew link python
        brew linkapps
        touch  ${PYTHON_FILE}.built
        export PYTHON_SYS="/usr/local/bin/python"
        return 0
    fi
}

# Install local Python from source
install_python_unix() {
    PYTHON_URL=https://www.python.org/ftp/python/${PYTHON_VER}/Python-${PYTHON_VER}.tgz
    PYTHON_FILE=Python-${PYTHON_VER}
    if [ -f ${PYTHON_FILE}.built ]; then 
        export PYTHON_SYS="${PYTHON_PATH}/bin/python"
        return 0 
    fi

    echo_green 
    echo_green "Getting Python ${PYTHON_VER} from ${PYTHON_URL}."
    echo_green

    [ -s ${PYTHON_FILE}.tar.gz ] || ${CURL} ${PYTHON_URL} > ${PYTHON_FILE}.tar.gz

    if [ -d ${PYTHON_FILE}/ ]; then
        echo_red "ERROR: ${PYTHON_FILE}/ already exists.  Inconsistent install state."
        return 1
    fi

    do_it() { 
        ( cd ${PYTHON_FILE}/                  &&
         ./configure --prefix=${PYTHON_PATH}  &&
         make                                 &&
         make install                         )
    }
    clean_up() {
        rm -rf ${PYTHON_FILE}/
    }
    tar xzf ${PYTHON_FILE}.tar.gz
    if do_it ; then
        clean_up
        export PYTHON_SYS="${PYTHON_PATH}/bin/python"
        touch  ${PYTHON_FILE}.built 
        return 0
    else
        echo_red "Error: Failed to install python!"
        clean_up
        return 1
    fi
}


# Run the appropriate installer
install_python() {  
    if is_mac ; then
        install_python_osx
    else
        install_python_unix 
    fi
}

activate_virtualenv() {
    echo_green
    echo_green "Activating VirtualEnv ${PYTHON_PATH}/${INITIAL_ENV}"
    echo_green
    source ${PYTHON_PATH}/${INITIAL_ENV}/bin/activate
}

install_virtualenv() {
    VIRTUALENV_URL=https://pypi.python.org/packages/source/v/virtualenv/virtualenv-${VIRTUALENV_VER}.tar.gz
    VIRTUALENV_FILE=virtualenv-${VIRTUALENV_VER}
    if [ -f ${VIRTUALENV_FILE}.built ]; then return 0; fi;

    echo_green
    echo_green "Getting VirtualEnv ${VIRTUALENV_VER} from ${VIRTUAL_ENV_URL}."
    echo_green
    [ -s ${VIRTUALENV_FILE}.tar.gz ] || ${CURL} ${VIRTUALENV_URL} > ${VIRTUALENV_FILE}.tar.gz
    if [ -d ${VIRTUALENV_FILE}/ ]; then
        echo_red "ERROR: ${VIRTUALENV_FILE}/ already exists.  Inconsistent install state."
        return 1
    fi

    tar xzf ${VIRTUALENV_FILE}.tar.gz
    do_it() { 
        ( cd ${VIRTUALENV_FILE}/                    && 
          ${PYTHON_SYS} virtualenv.py               \
                ${PYTHON_PATH}/${INITIAL_ENV}       )
    }
    clean_up() {
        rm -rf ${VIRTUALENV_FILE}/
    }
    if do_it ; then
        touch ${VIRTUALENV_FILE}.built
        clean_up
        return 0
    else
        echo_red "Error setting up VirtualEnv!"
        clean_up
        return 1
    fi
}


# Adjust $HOME/.bashrc to activate the virtualenv.
append_bashrc() {
    if [ -f bashrc_updated.built ] || ! modify_global_is_set ; then return 0; fi

    BASHRC="${HOME}/.bashrc"
    cat <<EOF >> ${BASHRC}

# Added by Toly's python installer.
if [ -f ${PYTHON_PATH}/${INITIAL_ENV}/bin/activate ]; then
    VIRTUAL_ENV_DISABLE_PROMPT=1
    source ${PYTHON_PATH}/${INITIAL_ENV}/bin/activate && export PATH="${PYTHON_PATH}/${INITIAL_ENV}/bin:\$PATH" 
fi
EOF

    touch bashrc_updated.built
}

# Install the pip packages
install_packages() {
    if [ -f pip_packages_installed.built ]; then return 0; fi

    if ! activate_virtualenv ; then echo_red "Couldn't activate VirtualEnv.!" ; return 1; fi;

    echo_green
    echo_green "Installing packages into VirtualEnv."
    echo_green
    
    #Ensure that ZeroMQ is installed for pyzmq (for ipython)
    if ! check_zeromq ; then echo_red  "ZeroMQ not found.!"  ; return 1; fi;

    #Ensure that gfortran is installed -- for numpy/scipy
    if ! check_gfortran ; then echo_red "gfortran not found.!"  ; return 1; fi;

    if is_mac ; then
        if is_mavericks ; then
            # Flag needed on OS X 10.9 Mavericks, for scipy to build
            export  CPPFLAGS="-D__ACCELERATE__ ${CPPFLAGS}"
        fi
    fi
    pip_modules=(   \
        multiprocessing # General nice to haves
        joblib
	readline
	lxml
        beautifulsoup4
        urllib3
        psycopg2
        pytidylib
        simplejson
    	requests
	requests_oauthlib
	sqlalchemy
	pygments
	actdiag
	blockdiag
	seqdiag
	xlrd	
     	jinja2	    # ipython family                    
        markupsafe		
        tornado		
        pygments		
        pyzmq			
        ipython		
	ipython_sql
 	numpy	    # scipy family                      
        scipy			
        scikit-learn		
        pandas
        us   # other
        markdown2
        unidecode
        folium
        patsy
        quandl
        statsmodels
        networkx
        nltk
        flask  # Flask
        flask-restful
    )
    for m in ${pip_modules[@]}; do
        if ! pip install $m ; then echo_red "Error installing $m with pip!" ; return 1; fi;
    done

    install_matplot_lib    

    install_pydot

    touch pip_packages_installed.built
}

install_pydot() {
    # Needs special version of pyparsing
    pip install -U pydot  pyparsing==1.5.7
}

install_matplot_lib() {
    #Ensure that Freetype/libpng are installed for matplotlib
    if ! check_freetype ; then echo_red  "freetype not found.!"  ; return 1; fi;
    if ! check_libpng ; then echo_red  "libpng not found.!"  ; return 1; fi;
 
    # Matplotlib seems to need special care on OS X:
    if is_mac ; then
        export ADD_CFLAGS=`pkg-config --cflags --libs libpng freetype2`
        export CFLAGS="${ADD_CFLAGS} ${CFLAGS}"
        
        # We must use clang on OS X, to compile the OS X backend.
        # In any case, ipython notebook --matplotlib=inline still works.
        if [ ! `gcc --version | grep clang` ]; then
            export CC="clang"
        fi
    fi

    pip install matplotlib

    # Initialize ~/.ipython if needed.
    echo "quit" | ipython console > /dev/null 2>&1

    cat <<EOF >> $HOME/.ipython/profile_default/ipython_notebook_config.py
c = get_config()
c.IPKernelApp.matplotlib = 'inline'
EOF
}

# Verify that the user is willing to have non-local files modified.
prompt_modify_global() {
    echo "Warning: By default, this script may modify the following files outside of this python install:"
    echo "    - \$HOME/.bashrc;"
    is_mac &&  echo "    - Will install python (and zeromq/gfortran/etc. if not already installed) using homebrew.  In particular globally(!)."

    run_confirm() {
        read -p "Allow this (y/n)?" choice
        case "$choice" in 
            y|Y) 
                return 0;;
            n|N) 
                return 1;;
            *)
                echo "Invalid choice.  Try again"
                echo
                run_confirm;;
        esac
    }
    run_confirm
}

modify_global_is_set() {
    [ -f allow_modify_global.built ]
}



##########################
#  The main driver code  #
###

if ! build_in_progress ; then
    if [ -d ${PYTHON_PATH} ]; then
        echo_red "Error: ${PYTHON_PATH} already exists.  Quitting!"
        exit 1
    fi

    [ -d ${PYTHON_PATH}/ ]      || mkdir -p ${PYTHON_PATH}/
    [ -d build-${PYTHON_VER}/ ] || mkdir -p build-${PYTHON_VER}/

    #Disable for interactive use.
    #if prompt_modify_global ; then
    touch build-${PYTHON_VER}/allow_modify_global.built
    #fi
fi


cd build-${PYTHON_VER}/

# These functions rely on being in the build directory!
detect_curl || (  echo_red "Couldn't find curl / wget!"; exit 1  )
install_python
install_virtualenv
append_bashrc
install_packages
