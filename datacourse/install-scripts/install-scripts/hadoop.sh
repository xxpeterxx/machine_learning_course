#!/bin/bash
# Instaling Ubuntu
# http://www.cloudera.com/content/cloudera-content/cloudera-docs/CDH4/latest/CDH4-Quick-Start/cdh4qs_topic_3_2.html
set -e 
set -x

# JDK and mrjob  installed elsewhere.

# Install maven
sudo apt-get -y install maven

#####
# Cloudera config
# 
# Use the Ubuntu Precise (=12.04) versions
#
wget http://archive.cloudera.com/cdh4/one-click-install/precise/amd64/cdh4-repository_1.0_all.deb
sudo dpkg -i cdh4-repository_1.0_all.deb

# add cloudera public key to repository
curl -s http://archive.cloudera.com/cdh4/ubuntu/precise/amd64/cdh/archive.key | sudo apt-key add -

# install hadoop
sudo apt-get update
sudo apt-get -y install hadoop-0.20-conf-pseudo

# then check that everything is working correctly:
dpkg -L hadoop-0.20-conf-pseudo

# Install a logger, to eliminate some errors.
bash get-slf4j-logger.sh


######

# install scalding
sudo apt-get -y install scala
sudo apt-get -y install git
git clone https://github.com/twitter/scalding.git
cd scalding
./sbt update && ./sbt test && ./sbt assembly 

# Format the NameNode
sudo -u hdfs hdfs namenode -format

# Start HDFS
for x in `cd /etc/init.d ; ls hadoop-hdfs-*` ; do sudo service $x start ; done

# Create the /tmp Directory
sudo -u hdfs hadoop fs -mkdir /tmp 
sudo -u hdfs hadoop fs -chmod -R 1777 /tmp

# Create MapReduce system directories:
sudo -u hdfs hadoop fs -mkdir -p /var/lib/hadoop-hdfs/cache/mapred/mapred/staging
sudo -u hdfs hadoop fs -chmod 1777 /var/lib/hadoop-hdfs/cache/mapred/mapred/staging
sudo -u hdfs hadoop fs -chown -R mapred /var/lib/hadoop-hdfs/cache/mapred

# Verify HDFS File Structure
sudo -u hdfs hadoop fs -ls -R /

# Start MapReduce
for x in `cd /etc/init.d ; ls hadoop-0.20-mapreduce-*` ; do sudo service $x start ; done

# Create a Hadoop User directories
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export HDUSER=$(echo $DIR|cut -d"/" -f1)  ##Hack to figure out the user to do this for based on the directory
sudo -u hdfs hadoop fs -mkdir /user/$HDUSER
sudo -u hdfs hadoop fs -chown $HDUSER /user/$HDUSER

# Test Hadoop -- commented out for now
#hadoop fs -mkdir input
#hadoop fs -put /etc/hadoop/conf/*.xml input
#hadoop fs -ls input
#/usr/bin/hadoop jar /usr/lib/hadoop-0.20-mapreduce/hadoop-examples.jar grep input output 'dfs[a-z.]+'
#hadoop fs -ls
#hadoop fs -ls output
#hadoop fs -cat output/part-00000 | head

# Since your account is behind a firewall has been cutoff, run these commands from your local machine
# they forward digital ocean's localhost to your local one for the job, task tracker and namenode.
# then visit localhost:50030 on a browser to see the results ...
# ssh -f -N -L 50030:localhost:50030 -L 50060:localhost:50060 -L 50070:localhost:50070 user@digitalocean
