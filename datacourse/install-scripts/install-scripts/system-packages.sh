###
###This script has been completely converted to Chef scripts.
#!/bin/bash
set -e

sudo apt-get update

sudo apt-get -y install git parallel wget

# I want system (i)python to be around, even if there's a local one
sudo apt-get -y install python ipython    

# Build tools (enough to e.g., build local ipython)
sudo apt-get install -y build-essential make automake gcc pkg-config

# Libraries (ibid)
sudo apt-get install -y zlib1g-dev libssl-dev libreadline-dev libncursesw5-dev libncurses5-dev libgdbm-dev libsqlite3-dev libbz2-dev libxml2-dev libxslt-dev
sudo apt-get install -y libzmq3 libzmq3-dev  #For IPython
sudo apt-get install -y gfortran libblas-dev libatlas-dev liblapack-dev  #For Scipy
sudo apt-get install -y libfreetype6-dev  # For matplotlib
sudo apt-get install -y libtidy-dev #libtidy
sudo apt-get install -y graphviz

# Some DB goodness
sudo apt-get install -y sqlite
sudo apt-get install -y postgresql libpq-dev 

# And postgres -- set up a role for everyone in group sudo
for NAME in $( grep "^sudo" /etc/group | grep -o '[^:]*$' | tr ',' '\n' ); do
    su postgres -c "psql -c \"CREATE USER ${NAME} CREATEDB;\""
done

# For the Java ecosystem
sudo apt-get install -y default-jdk default-jre
