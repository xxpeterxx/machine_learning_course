#!/bin/bash
## Get rid of logger warnings
set -e
set -x

if ls /usr/lib/hadoop/slf4j-* >/dev/null ; then 
	echo "Already have logger"; exit 0;
else 
	curl http://www.slf4j.org/dist/slf4j-1.7.7.tar.gz > data/slf4j-1.7.7.tar.gz
	(cd data && tar xzf slf4j-1.7.7.tar.gz)
	sudo cp data/slf4j-1.7.7/slf4j-simple-1.7.7.jar /usr/lib/hadoop/
fi
