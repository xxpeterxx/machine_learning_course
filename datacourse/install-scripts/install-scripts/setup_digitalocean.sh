# To run this command:
# curl https://gist.githubusercontent.com/tianhuil/0aa9b265f55413dc7198/raw > setup_digitalocean.sh
# . setup_digitalocean.sh

# Update sudo apt-get
sudo apt-get update

# Installing scientific Python
echo "y\ny\ny\ny" | sudo apt-get install --fix-missing build-essential python-dev python-numpy python-setuptools python-scipy libatlas-dev
echo "y" | sudo apt-get install --fix-missing build-essential python-sklearn
echo "y" | sudo apt-get install --fix-missing build-essential ipython
echo "y" | sudo apt-get install python-pip
echo "y" | sudo apt-get install libxml2-dev libxslt1-dev  # lxml useful for parsing and python uses it

# Installing other Python packages.
pip install virtualenv
pip install pandas
pip install beautifulsoup4 html5lib lxml requests urllib urllib2 requests_cache


# Installing MySQL
sudo apt-get install mysql-server libmysqlclient-dev
sudo mysql_install_db
sudo /usr/bin/mysql_secure_installation
# decline to set a root password, but say yes to everything else.

# Installing emacs:
echo "y" | sudo apt-get install emacs24

# Lock down server with iptables:
sudo iptables -F
sudo iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 22 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 80 -j ACCEPT
sudo iptables -I INPUT 1 -i lo -j ACCEPT
sudo iptables -A INPUT -j DROP

# make iptables persist
echo "y" | sudo apt-get install iptables-persistent
sudo service iptables-persistent start

# fail2ban
# echo "y" | sudo apt-get install fail2ban
# sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
# sudo emacs /etc/fail2ban/jail.local
# See https://www.digitalocean.com/community/articles/how-to-protect-ssh-with-fail2ban-on-ubuntu-12-04
# sudo service fail2ban restart
# sudo iptables -L  # examine results

# this is more for demonstrating how to get around bugs on OSX:
# ARCHFLAGS=-Wno-error=unused-command-line-argument-hard-error-in-future sudo -E pip install lxml

# input.rc:
cat >~/.inputrc <<EOL
# By default up/down are bound to previous-history
# and next-history respectively. The following does the
# same but gives the extra functionality where if you
# type any text (or more accurately, if there is any text
# between the start of the line and the cursor),
# the subset of the history starting with that text
# is searched (like 4dos for e.g.).
# Note to get rid of a line just Ctrl-C
"\e[B": history-search-forward
"\e[A": history-search-backward
EOL

# to install email, run these commands.
# for more, checkout https://www.digitalocean.com/community/articles/how-to-install-and-setup-postfix-on-ubuntu-12-04
sudo apt-get install postfix
sudo apt-get install mailutils

# bash.rc:
cat >>~/.bashrc <<EOL
export HISTSIZE=1000000
export HISTIGNORE="&:ls:[bf]g:exit"
export HISTCONTROL=erasedups
shopt -s histappend  # Append to, rather than overwite, to history on disk  

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias mkdir='mkdir -p' # make parent directories as needed
EOL
