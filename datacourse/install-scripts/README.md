# Install script root

Explanation of file structure:

1. install-scripts/: This is the directory of install scripts that gets pushed to each new droplet.  The scripts on it get run to instantiate the new instance.

2. do/: This is the directory containing scripts for interfacting with Digital Ocean (DO).

3. run-all-do.sh: This is the driver script for making all the instances.  It makes a tarball of install-scripts/ and places it in do/, and then runs the main DO install driver ("do/do-all-one.py") for each user appearing in the file do/users-to-setup.csv.


# Usage:
In principle, all you need to do is run
>          
    run-all-do.sh

This will spawn many processes (one for each new droplet) that work on creating a single droplet.  The progress / output of the process goes to a file named username.out -- this can be used to detect when something went wrong, and to re-run it manually.  (Why is this necessary? Yikes..)
