#!/bin/bash
set -e
set -x

# Make the tarball that the scripts will push out.
tar czf do/install-scripts.tar.gz install-scripts

cd do/
for i in $(cat users-to-setup.csv|cut -d"," -f4|tail -n +2); do
    nohup python ./do-all-one.py $i >$i.out 2>&1 &
done;
