#!/usr/bin/python
import requests, csv, subprocess, sys

import creds

## Guard against running this accidentally
sys.exit(1)

drop_names = []
with open('users-to-setup.csv', 'r') as users_csv:
    user_reader = csv.DictReader(users_csv)
    for user in user_reader:
        drop_names.append(user['Username'])

res = requests.get( "https://api.digitalocean.com/v1/droplets/", params = { 'client_id' : creds.client_id, 'api_key' : creds.api_key } )
for r in res.json()['droplets']:
    if r['name'] in drop_names:
        print "About to drop ", r['name']
        rdrop = requests.get( "https://api.digitalocean.com/v1/droplets/{droplet_id}/destroy/".format(droplet_id=r['id']) , params = { 'client_id' : creds.client_id, 'api_key' : creds.api_key } )
        print rdrop.json()



