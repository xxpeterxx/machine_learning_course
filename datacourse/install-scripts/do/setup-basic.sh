#!/bin/bash
set -x

SCP="scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -q"
SSH="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"
IP="$1"
NAME="$2"
KEY="$3"
FULLNAME="$4"

## Somehow these scp transfers over DO are very unreliable.
#  So we retry..
while [ 1 ]; do
    rsync -avq --progress --inplace --rsh="$SSH" install-scripts.tar.gz root@$IP:
    if [ "$?" = "0" ]; then
        break
    fi
done
$SSH root@$IP "tar xzf install-scripts.tar.gz"
$SSH root@$IP "bash install-scripts/initial.sh \"$NAME\" \"$FULLNAME\" \"$KEY\""
