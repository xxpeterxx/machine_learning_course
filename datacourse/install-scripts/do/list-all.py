#!/usr/bin/python
import requests
import csv
import subprocess

import creds

res = requests.get( "https://api.digitalocean.com/v1/droplets/", params = { 'client_id' : creds.client_id, 'api_key' : creds.api_key } )
for r in res.json()['droplets']:
    print r
