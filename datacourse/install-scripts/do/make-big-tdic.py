#!/usr/bin/python
import requests
import csv
import subprocess
import time
from multiprocessing import Pool
import sys

import creds

def go_get_droplet(hostname):
    """
    Create a new droplet -- it if already exists, merely fetch it.
    """
    droplet = None
    while not droplet:
        res = requests.get( "https://api.digitalocean.com/v1/droplets/", params = { 'client_id' : creds.client_id, 'api_key' : creds.api_key } )
        droplets = [ d for d in res.json()['droplets'] if d['name']==hostname ]
        if len(droplets)==1:
            droplet = droplets[0]
        else:
            toly_ssh="127502"
            michael_ssh="100592"
            params = {
                'client_id' : creds.client_id,
                'api_key' : creds.api_key,
                'name' : hostname,
                'size_id' : '69', ## The 64GB instance.
                'image_id' : '3240036',
                'region_id' : '4',
                'ssh_key_ids' : ','.join([toly_ssh, michael_ssh])
            }
            res = requests.get( "https://api.digitalocean.com/v1/droplets/new", params=params )
            print "Creating new droplet ", hostname, ":", res
            time.sleep(1)
    return droplet


def go_basic(droplet):
    ready = False
    while droplet['locked']:
        rd = requests.get( "https://api.digitalocean.com/v1/droplets/{droplet_id}".format(droplet_id=droplet['id']), params = {'client_id' : creds.client_id, 'api_key' : creds.api_key } )
        if rd.json()['status'] != 'OK':
            print "Uh oh: Couldn't find information for droplet ", res['name']
        droplet = rd.json()['droplet']
        time.sleep(5)
    print "Droplet %s is ready!" % droplet['name']
    time.sleep(5)
    ip = droplet['ip_address']
    subprocess.call("bash ./setup-basic.sh {ip} \"{name}\" \"{key}\" \"{fullname}\"".format(ip=ip, key="", fullname="The Data Incubator", name="preygel"), shell=True)

def go_complete(droplet):
    ready = False
    while droplet['locked']:
        rd = requests.get( "https://api.digitalocean.com/v1/droplets/{droplet_id}".format(droplet_id=droplet['id']), params = {'client_id' : creds.client_id, 'api_key' : creds.api_key } )
        if rd.json()['status'] != 'OK':
            print "Uh oh: Couldn't find information for droplet ", res['name']
        droplet = rd.json()['droplet']
        time.sleep(5)
    print "Droplet %s is ready!" % droplet['name']
    time.sleep(5)
    ip = droplet['ip_address']
    subprocess.call("bash ./setup-complete.sh {ip} \"{name}\"".format(ip=ip, key="", name="preygel", fullname="The Data Incubator"), shell=True)


def do_create(hostname):
    """
    The whole creation sequence (incl. install scripts) for a new droplet.
    """
    droplet = go_get_droplet(hostname)
    time.sleep(30)
    go_basic(droplet)
    time.sleep(5)
    go_complete(droplet)

def do_drop(hostname):
    """
    Drops an existing DO instance -- looked up by name.
    """
    res = requests.get( "https://api.digitalocean.com/v1/droplets/", params = { 'client_id' : creds.client_id, 'api_key' : creds.api_key } )
    for r in res.json()['droplets']:
        if r['name'] == hostname :
            print "About to drop ", r['name']
            rdrop = requests.get( "https://api.digitalocean.com/v1/droplets/{droplet_id}/destroy/".format(droplet_id=r['id']) , params = { 'client_id' : creds.client_id, 'api_key' : creds.api_key } )
            print rdrop.json()


#######

hostname = "tdic1"
if len(sys.argv)<2 or (sys.argv[1].lower() not in ["create", "drop"]):
    print "Usage: %s [create|drop]"%sys.argv[0]
    sys.exit(1)

if sys.argv[1].lower() == "create":
    do_create(hostname)
elif sys.argv[1].lower() == "drop": 
    do_drop(hostname)
else:
    print "This is impossible!"
    sys.exit(1)
