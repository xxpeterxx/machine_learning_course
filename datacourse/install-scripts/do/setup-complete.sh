#!/bin/bash
set -x

SSH="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"
IP="$1"
NAME="$2"
$SSH root@$IP bash /home/$NAME/install-scripts/system-packages.sh
$SSH root@$IP su $NAME -l -c \"bash /home/$NAME/install-scripts/local-python/install-local-python.sh\"
$SSH root@$IP bash /home/$NAME/install-scripts/hadoop.sh
$SSH root@$IP su $NAME -l -c \"bash /home/$NAME/install-scripts/local-spark/install-local-spark.sh\"
