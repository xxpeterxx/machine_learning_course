# DO installer documentation

Explanation of various files:

0. install-scripts.tar.gz: This file is not in git and is not there by default.  However, this file is _required_ to be present in order for the main install driver ("do-all-one.py") below to function correctly.  It's intended to be a tarball of ../install-scripts/ and it is the user's responsibility to create it / update it.

1. creds.py: This file consists of something like
>       
    client_id='...'
    api_key='...'

containing the DO (API v1) credentials.

2. setup-basic.sh: This is run locally to (remotely) create a new user account on a new DO instance.  Usage:
>      
    bash setup-basic.sh "IP" "username" "sshkey" "Full Name"


3. setup-complete.sh: This is run locally to (remotely) run the rest of the install scripts for the DO instance (the ones that are, more-or-less, user-local -- these are still run as root for reasons of ssh keys).  Usage:
>        
    bash setup-complete.sh "IP" "username"

4. users-to-setup.csv: Read by most of the install scripts -- this has usernames / full names / SSH keys for the new users.

5. list-all.py: Lists all the droplets associated to the credentials.

6. do-all-one.py: This is the main driver for creating a single new user's droplet.

7. drop-one.py: This is the antidote to do-all-one, and drops a single new user's droplet.

8. drop-all.py, make-big-tdic.py, sizes.py: You can figure out what these do, but you shouldn't really use them. (drop-all is obvious, but I've made it safe by making it exit before doing anything; make-big-tdic was used to make the large tdic1 instance -- it is modelled in poor software design fashion after do-all-one.py; sizes.py gets the codes for the various sizes, because I didn't have access to the console)



