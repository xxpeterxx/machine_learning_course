#!/usr/bin/python
import requests
import csv
import subprocess
import sys

import creds


if len(sys.argv)<2:
    print "Usage: %s username"%sys.argv[0]
    sys.exit(1)

with open('users-to-setup.csv', 'r') as users_csv:
    user_reader = csv.DictReader(users_csv)
    user_info = dict( [ (user['Username'], user) for user in user_reader ] )
who = sys.argv[1]
if who not in user_info:
    print "User %s not found?" % (who,)
    sys.exit(1)
user = user_info[who]

res = requests.get( "https://api.digitalocean.com/v1/droplets/", params = { 'client_id' : creds.client_id, 'api_key' : creds.api_key } )
for r in res.json()['droplets']:
    if r['name'] == who :
        print "About to drop ", r['name']
        rdrop = requests.get( "https://api.digitalocean.com/v1/droplets/{droplet_id}/destroy/".format(droplet_id=r['id']) , params = { 'client_id' : creds.client_id, 'api_key' : creds.api_key } )
        print rdrop.json()
