#!/usr/bin/python
import requests
import csv
import subprocess
import time
from multiprocessing import Pool
import sys

import creds

# Fetch the droplet, create if needed.

def go_get_droplet(user):
    droplet = None
    while not droplet:
        res = requests.get( "https://api.digitalocean.com/v1/droplets/", params = { 'client_id' : creds.client_id, 'api_key' : creds.api_key } )
        droplets = [ d for d in res.json()['droplets'] if d['name']==who ]
        if len(droplets)==1:
            droplet = droplets[0]
        else:
            toly_ssh="127502"
            michael_ssh="100592"
            params = {
                'client_id' : creds.client_id,
                'api_key' : creds.api_key,
                'name' : user['Username'],
                'size_id' : '64',
                'image_id' : '5141286', # '3240036' -- this was the old id!
                'region_id' : '4',
                'ssh_key_ids' : ','.join([toly_ssh, michael_ssh])
            }
            res = requests.get( "https://api.digitalocean.com/v1/droplets/new", params=params )
            print "Creating new droplet ", user['Username'], ":", res
            time.sleep(1)
    return droplet


def go_basic(droplet):
    """ Run setup-basic.sh as root, so that the user can log-in """
    ready = False
    while droplet['locked']:
        rd = requests.get( "https://api.digitalocean.com/v1/droplets/{droplet_id}".format(droplet_id=droplet['id']), params = {'client_id' : creds.client_id, 'api_key' : creds.api_key } )
        if rd.json()['status'] != 'OK':
            print "Uh oh: Couldn't find information for droplet ", res['name']
        droplet = rd.json()['droplet']
        time.sleep(5)
    print "Droplet %s is ready!" % droplet['name']
    time.sleep(5)
    ip = droplet['ip_address']
    user = user_info[droplet['name']]
    subprocess.call("bash ./setup-basic.sh {ip} \"{name}\" \"{key}\" \"{fullname}\"".format(ip=ip, key=user['Public key'], name=user['Username'], fullname=user['Full Name']), shell=True)

def go_complete(droplet):
    """ This is mostly installation """
    ready = False
    while droplet['locked']:
        rd = requests.get( "https://api.digitalocean.com/v1/droplets/{droplet_id}".format(droplet_id=droplet['id']), params = {'client_id' : creds.client_id, 'api_key' : creds.api_key } )
        if rd.json()['status'] != 'OK':
            print "Uh oh: Couldn't find information for droplet ", res['name']
        droplet = rd.json()['droplet']
        time.sleep(5)
    print "Droplet %s is ready!" % droplet['name']
    time.sleep(5)
    ip = droplet['ip_address']
    user = user_info[droplet['name']]
    subprocess.call("bash ./setup-complete.sh {ip} \"{name}\"".format(ip=ip, key=user['Public key'], name=user['Username'], fullname=user['Full Name']), shell=True)

###

if len(sys.argv)<2:
    print "Usage: %s username"%sys.argv[0]
    sys.exit(1)

with open('users-to-setup.csv', 'r') as users_csv:
    user_reader = csv.DictReader(users_csv)
    user_info = dict( [ (user['Username'], user) for user in user_reader ] )
who = sys.argv[1]
if who not in user_info:
    print "User %s not found?" % (who,)
    sys.exit(1)
user = user_info[who]

droplet = go_get_droplet(user)
time.sleep(60)
go_basic(droplet)
time.sleep(5)
go_complete(droplet)
