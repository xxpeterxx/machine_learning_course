#!/bin/bash

ln -s -f ../../.git_hooks/pre-commit .git/hooks/pre-commit

## Install the filter
# GIT_UTIL_PATH=$(git rev-parse --show-toplevel)/.git_utils
# git config  --local filter.dropoutput_ipynb.clean   ${GIT_UTIL_PATH}/ipynb_output_filter.py
# git config  --local filter.dropoutput_ipynb.smudge  cat
git config  --local merge.renormalize true

## git pull normally does a fetch and a merge.
## This sets it to do a fetch and a rebase to avoid the merge
## https://coderwall.com/p/tnoiug
git config --local pull.rebase true
