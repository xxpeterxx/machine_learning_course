#!/usr/bin/env python
"""
Based on code  from https://gist.github.com/minrk/6160067

Test an ipynb by progessively executing code cells
Raises an exception with traceback if error is encountered.
How to use: `test_ipynb.py foo.ipynb bar.ipynb`
"""

import argparse
import unittest
import io
import os
import re
import sys
from Queue import Empty
from test_ipynb_links import TestLinks
try:
  from IPython.kernel import KernelManager # 1.0
except ImportError:
  from IPython.zmq.blockingkernelmanager import BlockingKernelManager # 0.13

from IPython.nbformat.current import read, write, NotebookNode

# error messages must be cleared of color, or they won't display properly in unittest
ansi_escape = re.compile(r'\x1b[^m]*m')
def remove_color(string):
  return ansi_escape.sub('', string)

class TestNotebook(unittest.TestCase):

  def __init__(self, testname, filename):
    super(TestNotebook, self).__init__(testname)
    print "running %s" % ipynb
    self.filename = filename
    self.olddir =  os.path.dirname(os.path.realpath(__file__))
    print self.olddir
    with io.open(filename, encoding='utf8') as f:
      self.nb = read(f, 'json')

  def setUp(self):
    #make sure we are in the correct directory 
    os.chdir("../lessons")

    # run %matplotlib inline -- *not* pylib.
    self.km = KernelManager()
    self.km.start_kernel(extra_arguments=['--matplotlib=inline'], stderr=open(os.devnull, 'w'))
    try:
      self.kc = self.km.client()
    except AttributeError:
      # 0.13
      self.kc = self.km
    self.kc.start_channels()

  def tearDown(self):
    #make sure we are in the original directory
    os.chdir(self.olddir)
    self.km.shutdown_kernel()
    del self.km

  def test_code_cell(self, cell):
    shell = self.kc.shell_channel
    iopub = self.kc.iopub_channel
    shell.execute(cell.input)
    shell.get_msg(timeout=60*60)  # wait for finish, maximum 60 min

    while True:
      try:
        msg = iopub.get_msg(timeout=0.2)
      except Empty:
        break
      if msg['msg_type'] == 'pyerr':
        content = msg['content']
        return "\n".join([remove_color(traceback) for traceback in content['traceback']])
    return None

  def test_notebook(self):
    self.kc.shell_channel.execute("pass")
    self.kc.shell_channel.get_msg()
    while True:
      try:
        self.kc.iopub_channel.get_msg(timeout=1)
      except Empty:
        break

    for ws in self.nb.worksheets:
      for cell in ws.cells:
        if cell.cell_type == 'code':
          ret = self.test_code_cell(cell)
          if ret is not None:
            print >> sys.stderr, "=" * 40
            print >> sys.stderr, "Error in", self.filename
            print >> sys.stderr, ret
            raise Exception

if __name__ == '__main__':
  suite = unittest.TestSuite()
  parser = argparse.ArgumentParser()
  parser.add_argument('ipynbs',  nargs='+', help='the notebooks to test')
  parser.add_argument("-l", action='store_true', help="test links only")
  args = parser.parse_args()
  for ipynb in args.ipynbs:
    suite.addTest(TestLinks("test_links", ipynb))
    if not args.l:
      suite.addTest(TestNotebook("test_notebook", ipynb))
  result = unittest.TextTestRunner().run(suite)

  #i.e. if any errors or failures occurred
  if (len(result.errors) + len(result.failures)) > 0:
      sys.exit(1)
