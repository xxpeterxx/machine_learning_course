import unittest
import io
import requests
import simplejson
import os
from multiprocessing import Pool
import copy_reg
import types
import markdown
from bs4 import BeautifulSoup
from IPython.nbformat.current import read, write, NotebookNode
from urlparse import urlparse
import re
from collections import namedtuple

def _pickle_method(method):
  func_name = method.im_func.__name__
  obj = method.im_self
  cls = method.im_class
  return _unpickle_method, (func_name, obj, cls)

def _unpickle_method(func_name, obj, cls):
  for cls in cls.mro():
    try:
      func = cls.__dict__[func_name]
    except KeyError:
      pass
    else:
      break
  return func.__get__(obj, cls)

Link = namedtuple('Link', ['url', 'text'])


class TestLinks(unittest.TestCase):

  def __init__(self, testname, filename):
    self.olddir = os.path.dirname(os.path.realpath(__file__))
    self.filedir = os.path.dirname(os.path.realpath(filename))
    super(TestLinks, self).__init__(testname)
    print "running %s" % filename
    self.filename = filename
    with io.open(filename, encoding='utf8') as f:
      self.nb = read(f, 'json')

  def setUp(self):
    os.chdir(self.filedir)
    copy_reg.pickle(types.MethodType, _pickle_method, _unpickle_method)

  def tearDown(self):
    os.chdir(self.olddir)

  def _test_web_link(self, link):
    resp = requests.get(link.url, verify=False)  # https://hive.apache.org has bad certificate
    test_val = ''
    self.failIf(resp.status_code != 200,
      "Web link failed %s : %s : %s" % (self.filename, link, resp.status_code)
    )

  def test_links(self):
    print "Testing on %s now" % self.filename
    for ws in self.nb.worksheets:
      links_list = [self.parse_md_and_get_links(cell.source)
        for cell in  ws.cells if cell.cell_type == 'markdown']

      web_links = [link for links in links_list for link in links['web']]
      print "%s: testing %d web links" % (self.filename, len(web_links))
      workers = Pool(max(1,min(len(web_links)/2, 30)))
      workers.map(self._test_web_link, web_links)

      local_links = [link for links in links_list for link in links['local']]
      print "%s: testing %d local links" % (self.filename, len(local_links))


      for link in local_links:
        test_val = ''
        file_name = re.sub("^/files/", '../', link.url)
        self.failIf(not os.path.isfile(file_name),
          "Local link failed %s : %s " % (self.filename, link)
        )

        self.assertEqual(test_val, '')


  def parse_md_and_get_links(self, cell_data):
    html = markdown.markdown(cell_data)
    parsed = BeautifulSoup(html)
    links = [Link(link.get('href'), link.text) for link in parsed.find_all('a')] +\
            [Link(link.get('src'), link.text) for link in parsed.find_all('img')]
    
    local_links = []
    web_links = []
    for link in links:
      parsed = urlparse(link.url)
      if parsed.scheme == "http" or parsed.scheme == "https":
        web_links += [link]
      else:
        local_links += [link]
    
    return {"web" : web_links, "local": local_links}
