set -e      ## Exit on error code

dir=`dirname $0`;

if [ ! -f $dir/test_ipynb.py ]; then
    echo "Error: .git_utils/clean_ipynb.sh not found!" >&2
    exit 1
fi

usage() {
cat <<EOL
  OPTIONS: 
  -a test all
  -c test course
  -o only links
  -h display this message 
EOL
}

LINKS_ONLY=
ALL=
COURSE=
while getopts "alhc" OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
      ;;
    l)
      LINKS_ONLY="-l" 
      ;;
    a) ALL=true
       ;;
    c) COURSE=true
       ;;
  esac
done

shift $(($OPTIND - 1))

if [[ "$ALL" == true ]];
then
	echo "Testing all ipython notebooks"
	find . -iname '*.ipynb' | \
		grep -v "ipynb_checkpoints" | \
		grep -v "PlayPen" | \
		tr '\n' '\0' | \
		xargs -0 $dir/test_ipynb.py $LINKS_ONLY
elif [[ "$COURSE" == true ]];
then
	echo "Testing ipynbs that the Course file links to."
	grep -o "lessons/[^\.]*.ipynb" Course.ipynb | \
		xargs test/test_ipynb.py $LINKS_ONLY

elif [[ -z $1 ]] ;
then
    output=$(git diff --cached --name-only --diff-filter=ACM | tr " " "\n" | grep '\.ipynb' || true)
    if [ -z "$output" ] ; then
      echo "No ipython notebooks edited."
    else
      echo "Testing \n $output"
      echo $output | xargs $dir/test_ipynb.py $LINKS_ONLY
    fi 
else
	echo "Testing ipython notebooks: $@"
	$dir/test_ipynb.py $LINKS_ONLY $@
fi
