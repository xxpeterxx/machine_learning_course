echo "Stashing any uncommitted changes..."
git stash -q --keep-index

echo "Stashed. Testing committed code now..."


test/test_ipynb.sh

return_code=$?

if [[ $return_code != 0 ]]; then
  echo "================================"
  echo "Something went wrong in testing."
  echo "Bringing back stashed changes and cancelling commit."
  git stash pop -q
  
  #exiting a hook on a nonzero return code cancels the commit.
  exit $return_code
fi



echo "Test Succeeded!" #if vacuously
echo "Bringing back stashed changes and accepting commit."
git stash pop -q
