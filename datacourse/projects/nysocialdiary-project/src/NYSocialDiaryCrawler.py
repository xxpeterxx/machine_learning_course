## Copied verbatim from week 6, changed to dump to stdout
import urllib2, urllib, urlparse, pprint, os, json, gzip, httplib, sys
from bs4 import BeautifulSoup
from joblib import Parallel, delayed


class Crawler(object):
    def __init__(self, url):
        self.url = url
        self.page = urllib2.urlopen(url)
        self.soup = BeautifulSoup(self.page)


class IndexCrawler(Crawler):
    def url_dates(self):
        # extracting the two and zipping is pretty hacky but appears to work
        urls = [urlparse.urljoin(self.url, el.get("href")) for el in self.soup.select('.archivelink')]
        dates = [el.text for el in self.soup.select('.archivedate')]
        return zip(urls, dates)

class PageCrawler(Crawler):
    def __init__(self, url, date):
        self.url = url
        self.date = date
        super(PageCrawler, self).__init__(url)

    def captions(self):
        return [el.text for el in self.soup.select('.photocaption')]

    def strongs(self):
        return [el.text for el in self.soup.select('strong')]

    def fonts(self):
        return [el.text for el in self.soup.select('font')]

    def json(self):
        return {
              "url": self.url,
              "date": self.date,
              "captions": self.captions(),
              "strongs": self.strongs(),
              "fonts": self.fonts(),
               }

    @classmethod
    def error_json(cls, url, date):
        return {
              "url": url,
              "date": date,
              "captions": [],
              "strongs": [],
              "fonts": []
               }

#####
if __name__ == "__main__":
    INDEX_URL = "http://www.newyorksocialdiary.com/node/281/?tid=59"
    url_dates = IndexCrawler(INDEX_URL).url_dates()

    def fetch_and_safe(url, date):
        print >> sys.stderr, "Fetching from", url, date
        try:
            return PageCrawler(url, date).json()
        except httplib.IncompleteRead:
            return PageCrawler.error_json(date)

    json_data = Parallel(n_jobs=10)(delayed(fetch_and_safe)(url, date) for url, date in url_dates)
    json.dump(json_data, sys.stdout)

