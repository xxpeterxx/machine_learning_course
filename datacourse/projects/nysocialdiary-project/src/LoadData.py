# Based on Michael's code from ~/datacourse/Week6/NYSocialDiary/LoadData.py
import os, simplejson, gzip, re, collections

## N.B. \s = whitespace in Python's regex syntax
REMOVE_LEADING_AND = re.compile(r'^and\s+(.*)')
def refine_name(name):
    # remove leading and: probably from "John Smith, John Doe, and Mary Smith"
    res = REMOVE_LEADING_AND.search(name)
    if res is not None:
        name = res.group(1)

    # find internal 'and', likely from two sources
    names = [n.strip() for n in name.split(" and ")]

    # handle case like e.g. 'Ashley and Ben Renshaw' and 'Mr. and Mrs. John Doe'   
    if len(names) == 2 and ' ' not in names[0]:
        common_last_name = ' '.join(names[1].split(' ')[1:])
        return [names[0] + ' ' + common_last_name, names[1]]
    else:
        return names

REMOVE_NON_WORD = re.compile(r'^\s*(\S.*\S)\s*$')
REMOVE_DR_MR = re.compile(r'^[DM]rs?[.]\s*(\S.*)$')
def clean_name(name):
    name = name.strip()

    res = REMOVE_NON_WORD.search(name)
    if res is not None:
        name = res.group(1)
    else:
        return ""
    
    res = REMOVE_LEADING_AND.search(name)
    if res is not None:
        name = res.group(1)
    
    res = REMOVE_DR_MR.search(name)
    if res is not None:
        name = res.group(1)

    return name

def is_name(name):
    name = name.strip()
    return (name != "Click here for NYSD Contents" and 
        name != "Jr." and 
        name != "M.D." and name != "MD" and
        name != "friend" and name != "friends" and name != "and friend" and name != "and friends" and
        name != '' and
        not name.startswith("Photographs by ")
    )


def tokenize_caption(caption):
    """
    Takes a string of names and tokenizes:

    A few corner cases to consider:
    u'Graham Ernst, Rita Ruggiero, Luke Montgomery, and Ashley and Ben Renshaw' (should include 'Ashley Renshaw' and 'Ben Renshaw')
    u'P Diddy and Kanye West'  (no comma and P is just a letter)
    """
    caption = caption.replace(u'\xa0', u' ') # Replace non-breaking space by ordinary space.
    names = [name.strip() for name in caption.split(",")]
    names = [clean_name(name) for name0 in names for name in refine_name(name0)]
    return [name for name in names if is_name(name)]


######

def read_attendee_lists():
    filename="data/data.json.gz"
    with gzip.open(filename, 'rb') as fp:
            json_data = simplejson.load(fp)

    attendee_lists = [tokenize_caption(caption) for page in json_data for caption in page["captions"]]
    return attendee_lists
    
