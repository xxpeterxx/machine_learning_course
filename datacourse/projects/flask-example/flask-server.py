### The next three cells will setup the backend code for our server
import gzip, json, itertools, collections
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from flask import Flask
from flask.ext import restful

####### Loading ########
rows = map(json.loads, gzip.open("../yelp-project/data/raw/yelp_academic_dataset.json.gz").readlines())

print "Loaded rows"

user_df = pd.DataFrame([r for r in rows if r['type'] == 'user'])
review_df = pd.DataFrame([r for r in rows if r['type'] == 'review'])
business_df = pd.DataFrame([r for r in rows if r['type'] == 'business'])

print "Loaded into pandas"

# Build the directed graph of reviews.  For simplicity we'll restrict to popular (and positive) ones.
biz = set( business_df[business_df.review_count > 30]['business_id'] )
usr = set( user_df[user_df.review_count > 500]['user_id'] )
revs = review_df[ review_df['user_id'].apply(lambda x: x in usr) & review_df['business_id'].apply(lambda x: x in biz)  ]

print "Filtered pandas"

G = nx.Graph()

to_usr = lambda x: ('u', x)
to_biz = lambda x: ('b', x)
G.add_nodes_from(map(to_biz, biz))
G.add_nodes_from(map(to_usr, usr))
#G.add_edges_from( zip(revs['user_id'], revs['business_id'] ))
G.add_weighted_edges_from( zip(
                        map(to_usr, revs['user_id']), 
                        map(to_biz, revs['business_id']),
                        revs['stars'])  )

print "Loaded graph"

###### API calls #########
user_list = [ {'user_id':uid, 'name': name}  for (uid,name) in user_df[['user_id','name']].values if uid in usr ]

def get_recommendations(target_user):
    personalization = { k : 0 for k in G.nodes() }
    personalization[to_usr(target_user)] = 1.0
    pr = nx.pagerank_scipy(G, personalization = personalization, max_iter=100)
    
    already_seen = set( review_df [ review_df.user_id == target_user ]['business_id'] )
    new_biz = sorted( [ {'business_id': k[1], 'score': v} for k,v in pr.iteritems() if v>1e-3 and k[0]=='b' and k[1] not in already_seen],
                     key = lambda x: x['score'], 
                     reverse = True)

    return new_biz[:15]



###### Flask server ############
app = Flask(__name__, static_folder='static', static_url_path='')
api = restful.Api(app)

## Hook up users endpoint
class UserList(restful.Resource):
    def get(self):
        return user_list
api.add_resource(UserList, '/users')
    
## Hook up users recommendation endpoint
class Recommend(restful.Resource):
    def get(self, user_id):
        return get_recommendations(user_id)
api.add_resource(Recommend, '/recommend/<string:user_id>')

## Start the server -- listening on all IPs
app.run(host='0.0.0.0')
