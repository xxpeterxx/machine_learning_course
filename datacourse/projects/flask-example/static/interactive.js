var app = angular.module('myTestInteractiveApp', ['ngResource']);

app.controller('Interact', ['$scope', 'Users', 'Recommendation', function($scope, Users, Recommendation) {
    // Instantiate an object to store your scope data in (Best Practices)
    $scope.data = {};
   
    $scope.data.current_user = null;
    $scope.data.users = Users.query();

    $scope.setUser = function(user_id) {
        console.log("! " + user_id);
        $scope.data.current_user = user_id;
        $scope.data.recs = Recommendation.query({}, {user_id : user_id}, function(res){
            console.log(res); 
        });
    };
}]);

app.factory('Users', function($resource){
    return $resource(
        '/users',
        {}
    );
});

app.factory('Recommendation', function($resource){
    return $resource(
        '/recommend/:user_id', 
        { user_id : '@user_id' }
    );
});


