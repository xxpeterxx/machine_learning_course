#!env python
titles = map(lambda l:l.strip(), open("/home/preygel/datacourse/Week5/spark-example/data/raw/titles-sorted.txt").readlines() )
apple_idx = titles.index('Apple,_Inc.')

def sp_link(s):
    s, ts = s.split(":")
    s = int(s)
    ts = map(int, ts.strip().split(" "))
    return (s,ts)

links = sc.textFile("/home/preygel/datacourse/Week5/spark-example/data/raw/links-simple-sorted.txt").map(sp_link)
rev_links = links.flatMap( lambda (s,ts): [(t,s) for t in ts] ).groupByKey()

first_rev = rev_links.take(10)

print [ list(t[1]) for t in first_rev]

