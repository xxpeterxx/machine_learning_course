#!/bin/bash
# Create a Hadoop User directories
set -e
set -x

export HDUSER=$USER
sudo -u hdfs hadoop fs -rmdir /user/$HDUSER || true
sudo -u hdfs hadoop fs -mkdir /user/$HDUSER || true
sudo -u hdfs hadoop fs -chown $HDUSER /user/$HDUSER
