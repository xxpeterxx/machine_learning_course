#!/bin/bash
set -e

PSQL="psql --set ON_ERROR_STOP=1"
NUMPROC=4
YEARS=`seq 1999 2012` 

if [ `uname` == "Darwin" ]; then
    ZCAT="gzcat"
else
    ZCAT="zcat"
fi

QUERY="COPY freddie_originations FROM stdin WITH DELIMITER '|' CSV NULL AS '';"
for y in ${YEARS} ; do for q in `seq 1 4`; do echo $q$y; done; done | parallel --max-procs=${NUMPROC}  "\
    echo 'Working on originations for {}';          \
    $ZCAT data/cut/freddie/historical_data1_Q{}.gz   \
        | ${PSQL} freddie_cut -c \"${QUERY}\"       "

QUERY="COPY freddie_performance FROM stdin WITH DELIMITER '|' CSV NULL AS '';"
for y in ${YEARS} ; do for q in `seq 1 4`; do echo $q$y; done; done | parallel --max-procs=${NUMPROC}  "\
    echo 'Working on performance for {}';               \
    $ZCAT data/cut/freddie/historical_data1_time_Q{}.gz  \
        | ${PSQL} freddie_cut -c \"${QUERY}\"           "

## Now that we've done the load, we can add the indexes
cat <<EOF | parallel --max-procs=${NUMPROC} "${PSQL} freddie_cut -c {}"
CREATE INDEX freddie_orig_start_idx ON freddie_originations (first_payment_date);
CREATE INDEX freddie_orig_purpose_idx ON freddie_originations (loan_purpose);
CREATE INDEX freddie_performance_loan_idx ON freddie_performance (loan_seqn);
CREATE INDEX freddie_performance_reporting_pd ON freddie_performance (reporting_pd);
CREATE INDEX freddie_zero_bal_idx ON freddie_performance (zero_balance_code);
EOF

# This could have been done in the ingestion above, but...
cat<<EOF | ${PSQL} freddie_cut
ALTER TABLE freddie_originations ADD COLUMN year INT;
ALTER TABLE freddie_originations ADD COLUMN month INT;
UPDATE freddie_originations SET 
    year=SUBSTRING(first_payment_date FROM 1 FOR 4)::INT,
    month=SUBSTRING(first_payment_date FROM 5 FOR 2)::INT;
ALTER TABLE freddie_originations ALTER COLUMN year SET NOT NULL;
ALTER TABLE freddie_originations ALTER COLUMN month SET NOT NULL;
EOF
