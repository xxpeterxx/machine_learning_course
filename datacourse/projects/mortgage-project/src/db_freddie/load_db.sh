#!/bin/bash

#
# This is a slightly complicated way to load the data.  It is optimizing for several unfortunate qualities of the data to be loaded:
#  - There's a lot of it.
#  - It requires pre-processing (here just via a Perl script, for some superficial issues..)
#  - There are bizarre errors that are (and should be) rejected by Postgres' \COPY.
#
# These mean that:
#  - For performance reasons, the desired behavior is to do everything in batches and in parallel.
#
#  - For error handling, we want to not accept bad records -- but we also don't want to terimate the
#    load (or even lost a batch) when they do occur.
#
# The present structure does the following to each part of the file in turn:
#    unzip |> split into batches |> run batches through Perl in parallel |> insert batches into DB in parallel
# and the last step implements "bisection" error handling: 
#    If a batch fails, try on the first and second half separately, until all good records are inserted and
#    all erroneous records are identified.
#
# This structure is, in fact, probably not terrible in general.  (The implementation has a few obvious drawbacks...)
#

set -e
PSQL="psql --set ON_ERROR_STOP=1"
NUMPROC=4
YEARS=`seq 2007 2012` 

for y in ${YEARS} ; do
    echo "Loading Freddie origination data for year $y"
    for q in `seq 1 4`; do
        echo "  .. Quarter $q"
        QUERY="COPY freddie_originations FROM stdin WITH DELIMITER '|' CSV NULL AS '';"
        BLOCK=5000
        TMPD=`mktemp -d "orig-pieces.XXXXXX"`

        # Unzip and split into pieces
        unzip -p  "data/raw/freddie/historical_data1_Q${q}${y}.zip" "historical_data1_Q${q}${y}.txt" | split -a6 -d -e -l $BLOCK - "${TMPD}/piece-"

        # Perl-filter each of the pieces
        find $TMPD -iname 'piece-*' | parallel --max-procs=${NUMPROC} --group "cat {} | perl -ple 's/([|]|^)\s*(?=$|[|])/\1/g;' | gzip > {}.filt.gz; rm {};"

        # Run the queries
        find $TMPD -iname 'piece-*.filt.gz' | parallel --max-procs=${NUMPROC}6 --group "src/db_freddie/bisect_error.sh \"{}\" \"${QUERY}\" ${BLOCK} || true"
       
        # Clear the directory
        rm -rf ${TMPD}
    done;
done;

for y in ${YEARS} ; do
    echo "Loading Freddie performance data for year $y"
    for q in `seq 1 4`; do
        echo "  .. Quarter $q"
        QUERY="COPY freddie_performance FROM stdin WITH DELIMITER '|' CSV NULL AS '';"
        BLOCK=500000
        TMPD=`mktemp -d "perf-pieces.XXXXXX"`
        
        # Unzip and split into pieces
        unzip -p  "data/raw/freddie/historical_data1_Q${q}${y}.zip" "historical_data1_time_Q${q}${y}.txt" | split -a6 -d -e -l $BLOCK - "${TMPD}/piece-"

        # Perl-filter each of the pieces
        find $TMPD -iname 'piece-*' | parallel --max-procs=${NUMPROC} --group "cat {} | perl -ple 's/([|]|^)\s*(?=$|[|])/\1/g;' | gzip > {}.filt.gz; rm {};"

        # Run the queries
        find $TMPD -iname 'piece-*.filt.gz' | parallel --max-procs=${NUMPROC} --group "src/db_freddie/bisect_error.sh \"{}\" \"${QUERY}\" ${BLOCK} || true"
       
        # Clear the directory
        rm -rf ${TMPD}
    done;
done;

## Now that we've done the load, we can add the indexes
echo "CREATE INDEX freddie_orig_state_idx ON freddie_originations (state);" | ${PSQL} freddie
echo "CREATE INDEX freddie_orig_purpose_idx ON freddie_originations (loan_purpose);" | ${PSQL} freddie
echo "CREATE INDEX freddie_performance_loan_idx ON freddie_performance (loan_seqn);" | ${PSQL} freddie
