#!/bin/bash

PSQL="psql --set ON_ERROR_STOP=1"

## Recursive.  Probably rather slow, but it's only on error (which is rare in our example)
bisect_error() {
    local FILE=$1
    local QUERY=$2
    local LEN=$3

    #echo "bisect_error \"$FILE\" \"$QUERY\" $LEN"

    do_it() {
        if [[ $LEN == 1 ]]; then
            zcat $FILE | ${PSQL} freddie -c "${QUERY}"
        else
            zcat $FILE | ${PSQL} freddie -c "${QUERY}" >/dev/null 2>&1
        fi
    }

    if [[ $LEN < 1 ]]; then
        return 0
    fi

    if do_it ; then
        rm $FILE
        return 0
    elif [[ $LEN == 1 ]]; then
        echo
        echo -n "ERROR loading: "
        zcat $FILE
        echo
        rm $FILE
        return 1
    else
        local LEN2=$(($LEN/2))

        #echo
        #echo "Splitting from $LEN to ${LEN2}"
        #echo

        zcat $FILE | head -n ${LEN2} | gzip > ${FILE}.0
        zcat $FILE | tail -n +$((1+$LEN2)) | gzip > ${FILE}.1
        rm $FILE

        local RET=0
        if ! bisect_error "${FILE}.0" "${QUERY}" ${LEN2} ; then RET=1; fi;
        if ! bisect_error "${FILE}.1" "${QUERY}" $(($LEN-$LEN2)) ; then RET=1; fi
        return $RET
    fi
}

bisect_error "$1" "$2" "$3"
