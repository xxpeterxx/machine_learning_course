DROP FUNCTION IF EXISTS pmt(double precision, double precision, int);
CREATE FUNCTION pmt(double precision, double precision, int) RETURNS double precision AS 
    'SELECT $1 * (1+$2)^$3 * ((1+$2) - 1)/((1+$2)^$3-1);' 
    LANGUAGE SQL
    IMMUTABLE
    RETURNS NULL ON NULL INPUT;

DROP FUNCTION IF EXISTS pmt_ann_30(double precision, double precision);
CREATE FUNCTION pmt_ann_30(double precision, double precision) RETURNS double precision AS
    'SELECT pmt($1, $2/12, 360);'
    LANGUAGE SQL
    IMMUTABLE
    RETURNS NULL ON NULL INPUT;

ALTER TABLE freddie_originations DROP COLUMN IF EXISTS Constant_Monthly_Payment;
ALTER TABLE freddie_originations ADD COLUMN Constant_Monthly_Payment double precision;
UPDATE freddie_originations SET Constant_Monthly_Payment = pmt_ann_30(orig_upb, orig_rate/100);

ALTER TABLE freddie_performance DROP COLUMN IF EXISTS Is_Prepay;
ALTER TABLE freddie_performance ADD COLUMN Is_Prepay int DEFAULT 0;
UPDATE freddie_performance SET Is_Prepay = 1 WHERE zero_balance_code = 1;

ALTER TABLE freddie_performance DROP COLUMN IF EXISTS Is_Default;
ALTER TABLE freddie_performance ADD COLUMN Is_Default int DEFAULT 0;
UPDATE freddie_performance SET Is_Default = 1 WHERE zero_balance_code=97 OR (2<=zero_balance_code AND zero_balance_code <= 8);

ALTER TABLE freddie_originations DROP COLUMN IF EXISTS Ends_Prepay;
ALTER TABLE freddie_originations ADD COLUMN  Ends_Prepay int DEFAULT 0;
UPDATE freddie_originations o SET Ends_Prepay = 1 FROM freddie_performance p WHERE o.loan_seqn = p.loan_seqn AND p.Is_Prepay = 1;

ALTER TABLE freddie_originations DROP COLUMN IF EXISTS Ends_Default;
ALTER TABLE freddie_originations ADD COLUMN  Ends_Default int DEFAULT 0;
UPDATE freddie_originations o SET Ends_Default = 1 FROM freddie_performance p WHERE o.loan_seqn = p.loan_seqn AND p.Is_Default = 1;

ALTER TABLE freddie_performance DROP COLUMN IF EXISTS past_delinq;
ALTER TABLE freddie_performance ADD COLUMN past_delinq int DEFAULT 0;
UPDATE freddie_performance p SET past_delinq=1
    FROM (
        SELECT loan_seqn, MIN(loan_age) AS min_age
            FROM freddie_performance 
            WHERE delinquency_status!='0' 
            GROUP BY loan_seqn
         ) d
    WHERE p.loan_seqn=d.loan_seqn AND p.loan_age >= d.min_age;
