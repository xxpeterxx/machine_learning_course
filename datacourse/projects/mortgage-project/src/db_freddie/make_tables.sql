DROP TABLE IF EXISTS freddie_originations;
CREATE TABLE freddie_originations (
    credit_score NUMERIC(3),
    first_payment_date CHAR(6) NOT NULL,
    first_time_buyer CHAR(1),
    maturity_date CHAR(6) NOT NULL,
    MSA NUMERIC(5),
    mortgage_insurance_pct NUMERIC(3),
    num_units NUMERIC(1),
    occupancy CHAR(1),
    CLTV REAL,
    DTI NUMERIC(3),
    orig_UPB REAL NOT NULL,
    LTV NUMERIC(3),
    orig_rate REAL NOT NULL,
    channel CHAR(1) NOT NULL,
    ppm_flag CHAR(1),
    product CHAR(5) NOT NULL,
    state CHAR(2) NOT NULL,
    property_type CHAR(2),
    postcode CHAR(5),
    loan_seqn CHAR(12) NOT NULL,
    loan_purpose CHAR(1) NOT NULL,
    orig_term NUMERIC(3) NOT NULL,
    num_borrowers NUMERIC(2),
    seller TEXT,
    servicer TEXT,
    PRIMARY KEY (loan_seqn)
);

DROP TABLE IF EXISTS freddie_performance;
CREATE TABLE freddie_performance (
    loan_seqn CHAR(12) NOT NULL,
    reporting_pd CHAR(6) NOT NULL,
    current_UPB REAL,
    delinquency_status CHAR(1),
    loan_age NUMERIC(3) NOT NULL,
    months_remaining NUMERIC(3) NOT NULL,
    repurchase_flag CHAR(1),
    mod_flag CHAR(1),
    zero_balance_code NUMERIC(2),
    zero_balance_date CHAR(6),
    cur_rate REAL,
    PRIMARY KEY (loan_seqn, reporting_pd)
);

-- The following we want, but don't do until after loading data.s
--CREATE INDEX freddie_orig_start_idx ON freddie_originations (first_payment_date);
--CREATE INDEX freddie_orig_state_idx ON freddie_originations (state);
--CREATE INDEX freddie_orig_purpose_idx ON freddie_originations (loan_purpose); 
--CREATE INDEX freddie_performance_loan_idx ON freddie_performance (loan_seqn); 
--CREATE INDEX freddie_performance_reporting_pd ON freddie_performance (reporting_pd);
--CREATE INDEX freddie_zero_bal_idx ON freddie_performance (zero_balance_code);
