#!/usr/bin/python
from bs4 import BeautifulSoup
import requests

cookies = {}
with open("src/freddie_cookie.nogit", "r") as f:
    for line in f:
        cookies[ line.split(" ")[-2] ] = line.split(" ")[-1].strip()

url = "https://freddiemac.embs.com/FLoan/Data/download.php"
url_base = "https://freddiemac.embs.com/FLoan/Data/"
target = "data/raw/freddie"
r = requests.get(url, cookies=cookies)
soup = BeautifulSoup( r.content )

##
print "set -e"
print "if [ ! -d data/ ]; then echo 'Error: data/ not found.'; exit 1; fi;"
print "if [ ! -d {target}/ ]; then mkdir -p {target}/; fi;".format(target=target)

for link in soup.find_all('a'):
    href = link['href']
    text = link.text
    if 'sample' in text:
        continue

    print 'if [ ! -f "%s/%s" ]; then' % (target, text)
    print '    curl -L "%s/%s" -b "%s=%s" > %s/%s' % (url_base, href, cookies.keys()[0], cookies.values()[0], target, text)
    print 'fi'
