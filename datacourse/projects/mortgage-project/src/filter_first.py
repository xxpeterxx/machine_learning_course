#!/usr/bin/python
import sys

def main():
    if len(sys.argv) < 2:
        print "Usage: %s file_with_filters" % (sys.argv[0],)
        return 1
    filename = sys.argv[1]
    hash = set([])
    with open(filename, 'r') as f:
        for l in f:
            hash.add(l.strip())

    for line in sys.stdin:
        first, _, _ = line.partition("|")
        if first in hash:
            sys.stdout.write( line )

if __name__ == "__main__":
    sys.exit(main())
