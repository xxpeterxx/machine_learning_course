#!/usr/bin/env python
import sys

def main():
    for line in sys.stdin:
        pieces = line.split('|')
        if pieces[16] == 'DE':
            print '|'.join([p.strip() for p in pieces])

if __name__ == "__main__":
    sys.exit(main())
