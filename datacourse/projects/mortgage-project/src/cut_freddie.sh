#!/bin/bash
set -e
set -p

if [ ! -d data/ ]; then echo "Error: data/ not found"; exit 1; fi
if [ ! -d data/cut/freddie ]; then mkdir -p data/cut/freddie; fi

YEARS=`seq 1999 2012` 
NUMPROC=4

if [ `uname` == "Darwin" ]; then
    ZCAT="gzcat"
else
    ZCAT="zcat"
fi

for y in ${YEARS} ; do for q in `seq 1 4`; do echo $q$y; done; done | parallel --max-procs=${NUMPROC}  "\
    echo 'Working on originations for {}';                  \
    unzip -p  data/raw/freddie/historical_data1_Q{}.zip     \
                historical_data1_Q{}.txt                    \
        | src/cut_freddie_orig.py                           \
        | gzip                                              \
        > data/cut/freddie/historical_data1_Q{}.gz   "

$ZCAT data/cut/freddie/historical_data1_Q*.gz | cut -d"|" -f20 > data/cut/freddie/freddie_loans_cut_orig.txt
            
for y in ${YEARS} ; do for q in `seq 1 4`; do echo $q$y; done; done | parallel --max-procs=${NUMPROC}  "\
    echo 'Working on performance for {}';                   \
    unzip -p  data/raw/freddie/historical_data1_Q{}.zip     \
                historical_data1_time_Q{}.txt               \
        | src/filter_first.py data/cut/freddie/freddie_loans_cut_orig.txt     \
        | gzip                                              \
        > data/cut/freddie/historical_data1_time_Q{}.gz    "

echo "Done!"
