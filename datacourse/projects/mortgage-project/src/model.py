import sqlalchemy as sql
import numpy as np
import pandas as pd
import getpass, random
import sklearn.linear_model
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import StandardScaler
import scipy.stats.mstats

engine = sql.create_engine("postgres://{user}@/freddie_cut".format(user=getpass.getuser()))
sample_rate = 0.1
oversample_defaults = 10
num_groups = 5

######

print "Running!"

df_orig = pd.read_sql("""
    SELECT loan_seqn, credit_score, msa, cltv, dti, 
            orig_upb, ltv, orig_rate, postcode, year, month, 
            constant_monthly_payment, ends_prepay, ends_default 
    FROM freddie_originations
    WHERE random() < {sample_rate} * (1 * (ends_default=0)::int + {oversample_defaults} * (ends_default=1)::int)
    """.format(sample_rate=sample_rate, oversample_defaults=oversample_defaults), engine)

print "Originations loaded."

def get_perf_by_origs(origs):
    orig_list = ",".join( ["'" + o + "'" for o in origs] )
    return pd.read_sql("""
        SELECT p.loan_seqn, p.reporting_pd, p.current_upb, p.delinquency_status, 
                p.loan_age, p.zero_balance_code, p.past_delinq, p.is_default, p.is_prepay, 
                o.constant_monthly_payment, o.orig_rate, o.credit_score, 
                o.dti, o.ltv, o.year, o.month, o.ends_default
        FROM freddie_performance p 
        INNER JOIN freddie_originations o 
        ON p.loan_seqn = o.loan_seqn
        WHERE p.loan_seqn IN ({0})
        """.format(orig_list), engine)
        
df_perf = get_perf_by_origs(df_orig['loan_seqn'].values)
df_perf["weight"] = (1-df_perf.ends_default) * 1.0 + df_perf.ends_default * 1.0 / oversample_defaults
del df_perf["ends_default"]
print "Performance loaded."

######

# Randomly split the originations into num_groups groups, to use in cross validation.
loan_group_hash = {}
random.seed(1758)
for x in df_orig.loan_seqn:
    loan_group_hash[x] = int( num_groups * random.random() )
 
def cut_data_on(feats, i):
    """
    Use the "loan_group_has" to partition the feature data into 90% training and 10% testing, (if num_groups=10)
    split by originations, using leave-one-out.
    
    The group i that's passed in is the testing set.
    """
    test  = feats[ feats.loan_seqn.apply(lambda x: loan_group_hash[x] == i) ].drop("loan_seqn", axis=1)
    train = feats[ feats.loan_seqn.apply(lambda x: loan_group_hash[x] != i) ].drop("loan_seqn", axis=1)
    return (train, test)


def train_with_logistic(train):
    """
    Do the training step for given training and testing features.
    """
    ## Train
    prepay_regression = sklearn.linear_model.LogisticRegression( ).fit(
        X = train.drop(["is_prepay", "is_default", "weight"], axis=1),
        y = train["is_prepay"])
    default_regression = sklearn.linear_model.LogisticRegression( ).fit(
        X = train.drop(["is_prepay", "is_default", "weight"], axis=1),
        y = train["is_default"])
    return (prepay_regression, default_regression)

def weighted_log_loss(truth, pred, weights):
    epsilon = 1e-15
    # Clip
    pred = map(lambda x: max(epsilon, x), pred)
    pred = map(lambda x: min(1-epsilon, x), pred)
    pred = np.array(pred)
    # Compute
    tot_w = sum(weights)
    return -weights.dot( (truth * np.log(pred) + (1-truth) * np.log(1-pred)) ) / tot_w

def test_on(test, p):
    """
    Takes testing data and a pair of regressions, and spits out (weighted) log loss
    """
    (prepay_regression, default_regression) = p
    
    ## Predict
    predicted_prepay, predicted_default = ( 
            map(lambda x: x[1], 
                prepay_regression.predict_proba( test.drop(["is_prepay", "is_default", "weight"], axis=1) ) ),
            map(lambda x: x[1], 
                default_regression.predict_proba( test.drop(["is_prepay", "is_default", "weight"], axis=1) ) ) 
    )

    return ( 
        weighted_log_loss( test["is_prepay"].values, predicted_prepay, test["weight"].values),
        weighted_log_loss( test["is_default"].values, predicted_default, test["weight"].values)
    )

def mean(l):
    return sum(l)/len(l)

def benchmark():
    def train_test_bench(train, test):
        """
        Do the train - predict - report metric step for given training and testing features.
        """
        
        # Calculate historical statistics 
        good_duration = 7.5*12
        prepay_by_age = pd.DataFrame( index=np.arange(good_duration) )
        default_by_age = pd.DataFrame( index=np.arange(good_duration)  )

        # Remember to adjust for oversampling of defaults..
        perft = df_perf[ (1999 <= df_perf.year) & (df_perf.year <= 2006) ]
        tot = len( df_orig[ (1999<=df_orig.year) & (df_orig.year<=2006) & (df_orig.ends_default==0) ]) + len( df_orig[ (1999<=df_orig.year) & (df_orig.year<=2006) & (df_orig.ends_default==1) ]) / oversample_defaults
        prepay_by_age["Total"]  = perft[ perft.is_prepay == 1 ].groupby(perft.loan_age)['loan_age'].count()*1.0/tot
        default_by_age["Total"] = perft[ perft.is_default == 1 ].groupby(perft.loan_age)['loan_age'].count()*1.0/tot/oversample_defaults

        prepay_by_age_cum = prepay_by_age.cumsum().interpolate().fillna(0)
        default_by_age_cum = default_by_age.cumsum().interpolate().fillna(0)
        default_by_age = default_by_age_cum.diff().fillna(0)
        prepay_by_age = prepay_by_age_cum.diff().fillna(0)

        # "Train"
        end_prepay = mean(prepay_by_age[(prepay_by_age.index>=58) & (prepay_by_age.index<=70)]['Total'])
        end_default = mean(default_by_age[(default_by_age.index>=58) & (default_by_age.index<=70)]['Total'])
        
        ## Predict
        predicted_prepay, predicted_default = ( 
                map(lambda age: prepay_by_age['Total'][age] if age<=70 else end_prepay, test.loan_age),
                map(lambda age: default_by_age['Total'][age] if age<=70 else end_default, test.loan_age)
        )

        return ( 
                weighted_log_loss( test["is_prepay"].values, predicted_prepay, test["weight"].values),
                weighted_log_loss( test["is_default"].values, predicted_default, test["weight"].values)
        )

    def generate_features_bench(perf):
        return perf[["loan_seqn", "loan_age", "is_prepay", "is_default", "weight"]]

    feats = generate_features_bench(df_perf)
    metrics = [train_test_bench(*cut_data_on(feats, i)) for i in range(num_groups)]
    print "Log-loss on prepays:", mean([m[0] for m in metrics])
    print "Log-loss on defaults:", mean([m[1] for m in metrics])

def naive():
    def generate_features(perf):
        feats = perf[["loan_seqn", "credit_score", "dti", "ltv", "loan_age", "past_delinq", "is_prepay", "is_default", "weight" ]]
        feats.dropna(how='any', inplace=True) 
        return feats

    feats = generate_features(df_perf)
    def do(train, test):
        return test_on(test, train_with_logistic(train))
    metrics = [do(*cut_data_on(feats, i)) for i in range(num_groups)]
    print "Log-loss on prepays:", mean([m[0] for m in metrics])
    print "Log-loss on defaults:", mean([m[1] for m in metrics])

def less_naive():
    ## An exampe of trying to 'bin' a variable (credit_score, ltv), and linearly spline another (age)
    ##
    ## I didn't try to optimize this very well -- and for defaults it does worse than the above!
    ## Nevertheless, it was worth a shot!
    def generate_features(perf):
        feats = perf[["loan_seqn", "is_prepay", "is_default", "weight" ]]

        # We believe these originations are somehow exceptionally bad.
        feats["fudge_origination"] = 1 * (2005 <= perf.year) * (perf.year <= 2007)

        # Bin the LTV
        feats["high_ltv"] = 1 * (perf.ltv > 80)
        feats["low_ltv"] = 1 * (perf.ltv < 70)

        # Bin the FICO score
        feats["fico4"] = 1 * (perf.credit_score > 800)
        feats["fico3"] = 1 * (perf.credit_score > 750)
        feats["fico2"] = 1 * (perf.credit_score > 700)
        feats["fico1"] = 1 * (perf.credit_score > 650)

        # Bin / linearly spline (in the middle) the age
        feats["1_age<=6"] = 1 * (perf.loan_age<=6)
        feats["1_6<age<=24"] = 1 * (6<perf.loan_age) * (perf.loan_age<=24)
        feats["6<age<=24"] = feats["1_6<age<=24"] * perf.loan_age
        feats["1_24<age<=72"] = 1 * (24<perf.loan_age)* (perf.loan_age<=72)
        feats["24<age<=72"] = feats["1_24<age<=72"] * perf.loan_age
        feats["1_age>72"] = 1 * (perf.loan_age>72)

        feats.dropna(how='any', inplace=True)
        return feats
   
    feats = generate_features(df_perf)
    def do(train, test):
        return test_on(test, train_with_logistic(train))
    metrics = [do(*cut_data_on(feats, i)) for i in range(num_groups)]
    print "Log-loss on prepays:", mean([m[0] for m in metrics])
    print "Log-loss on defaults:", mean([m[1] for m in metrics])

print
print "Running benchmark:"
benchmark()

print
print "Running naive:"
naive()

print
print "Running less naive:"
less_naive()
