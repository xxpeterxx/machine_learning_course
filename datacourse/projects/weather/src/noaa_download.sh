#!/usr/bin/bash
# for the list of stations
#   http://www1.ncdc.noaa.gov/pub/data/inventories/ISH-HISTORY.TXT
# for the list of columns
#   ftp://ftp.ncdc.noaa.gov/pub/data/noaa/isd-lite/isd-lite-format.pdf

set -e
set -x


TMPFILE=`mktemp /tmp/a.XXXXXXXXXX`

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"/../data/

download () {
	code=$1
	city=$2
	TMPFILE=`mktemp /tmp/$city.XXXXXXXXXX`
	for y in {2000..2012}
	do
		curl ftp://ftp.ncdc.noaa.gov/pub/data/noaa/isd-lite/$y/$code-$y.gz | gunzip -c >> $TMPFILE
	done
	gzip -c $TMPFILE > $DIR/$city.txt.gz
	rm -f $TMPFILE
}

download 725090-14739 boston
download 744860-94789 nyc
download 724080-13739 philadelphia
download 725300-94846 chicago
download 724060-93721 baltimore
