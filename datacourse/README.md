## General setup instructions for Fellows:
* Follow [these instructions] (http://tianhuil.github.io/datawiki/#!started/digital_ocean.md) to set up your digitalocean instance, including adding ssh keys to your DO instance and github account.

## Instructions for course developers (using Vagrant and Digitalocean)
* Ensure you have your `DO\_API\_KEY` environment variable set.
* Ensure that in the Vagrantfile, `override.ssh.private_key_path` points to the `vagrant_key` and that you have access to this key (ask Michael or Christian).

## Instructions for both digitalocean devs and intrepid virtualbox users:

* You do have [Vagrant] (https://docs.vagrantup.com/v2/installation/) installed, right?
* Run `./setup\_run\_vagrant.sh`.  Then follow the instructions at the end of this process.
* *(Digital Ocean ONLY)* After you've run this, follow the steps in the 'Workflow' section below to setup rsync. Then, run `rsync_digitalocean` or the similar alias you created.
* To update in the future, run `git pull` and then `vagrant provision`, which will re-install everything to spec. 
* Break something? First, try `vagrant provision`. If this fails, then *(making sure you have a local copy of your data)*, run `vagrant destroy`, then re-run `vagrant up` (possibly with the `--provider digital_ocean` flag.


## Rsync Workflow (for developers)

* Create a file called `hostname.txt` in the `.rsync_utils` directory, which contains the IP of your digitalocean instance (or ssh hostname, if you're fancy).

* Run setup\_rsync.sh and follow the instructions there to setup an rsync tool to your digital ocean (remote) box.

* Then whenever you update your files, just run rsync\_digitalocean on your local machine and it copy all your changes onto your remote machine.  Note that files on the local machine take precedence, so any changes on the remote will be erased if they conflict.

* *To test*, after doing the above, run `./test_via_rsync.sh` (or, to test all, `./test_via_rsync.sh all`)


## Chef Information (course developers):

Currently, the files in /cookbooks are in the repo for portability purposes. However, if you have Ruby installed, you can manually run an update to get the latest cookbooks for these installs: 
* `cd` into the `chef/` directory
* run `bundle install`
* run `librarian-chef install`

This will update all of the necessary cookbooks to the latest version.
