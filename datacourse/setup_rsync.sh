#/bin/bash

if [[ ! -e .rsync_utils/hostname.txt ]];
then
  >&2 echo "You do not have a .rsync_utils/hostname.txt file. You'll need to add the hostname or ip of your digitalocean instance to proceed." 
  >&2 echo "It's a one-liner, simply create a file and add *only* the IP address (or hostname). Again, there should be only one line." 
  exit 1
fi

DO_IP=$(head -n 1 .rsync_utils/hostname.txt)

echo ""
echo "alias rsync_digitalocean=\"rsync -r -a --exclude-from `pwd`/.rsync_utils/exclude.txt -e 'ssh -l vagrant' `pwd`/ $DO_IP:/vagrant/\""
echo ""

cat <<EOF > .rsync_utils/rsync_command.txt
function rsync_to_digitalocean {
  rsync -r -a --exclude-from `pwd`/.rsync_utils/exclude.txt -e 'ssh -l vagrant' `pwd`/ $DO_IP:/vagrant/
}
EOF

echo "Copy the above alias and insert it into your .bash_profile (osx) or .bashrc (linux, or you've modified your osx setup)"
echo "You can now run 'rsync_digitalocean', which syncs this directory to digitalocean"
