{
 "metadata": {
  "name": "",
  "signature": ""
 },
 "nbformat": 3,
 "nbformat_minor": 0,
 "worksheets": [
  {
   "cells": [
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "# What is Spark?\n",
      "Like _Hadoop_, __Spark__ is a low-level system for distributed computation on a cluster.  It has two major advantages over MapReduce:\n",
      " \n",
      " - It can do in-memory caching between stages, while Hadoop must write everything to disk.  This improves performance.  It also makes it suitable for classes of algorithms that would otherwise be too slow (e.g., iterative algorithms, like the training step in many machine learning algorithms).\n",
      " \n",
      " - It has a more flexible execution model (i.e., not just MapReduce).\n",
      " \n",
      "There are also minor advantages: The default API is much nicer than writing raw MR code.  Though the favored interface is via a language called Scala, there is also good support for using Python.  There do not seem to be clear disadvantages other than it being newer and thus slightly less tested.\n",
      "\n",
      "## How does Spark relate to Hadoop?  \n",
      "\n",
      "__Answer 1__: It's a replacement for it.  You can manage and run your own Spark cluster, independent of Hadoop.  You'll have to figure out the filesystem layer from scratch though.   (In this context, _Shark_ is the replacement for _Hive_, i.e., for SQL-like operation.)\n",
      "\n",
      "__Answer 2__: It's a complement to it.  You can run Spark on top of a Hadoop cluster, and still leverage HDFS and YARN -- then Spark is just replacing MapReduce.  (In this context, _Shark_ can be used as a drop-in replacement for _Hive_.)"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "## A primer on \"functional\" style for list processing\n",
      "\n",
      "It is Pythonic to operate on lists -- elementwise operations, filtering, etc. -- by using list comprehensions.  In many other languages, starting with Lisp but extending to many \"functional\" programming languages, a different style is preferred:\n",
      "\n",
      "The idea is that if `f` is a function, then one things of the application\n",
      ">          \n",
      "    list   |---->   [ f(x) for x in list ]\n",
      "\n",
      "on lists as a function of _two_ arguments: `f` and `list`.  The idea of viewing the function `f` as a parameter is typical in functional programming languages, and can be taken as a definition of the later term.\n",
      "\n",
      "Some common idioms in this style, with Pythonic equivalents, are:\n",
      "\n",
      "- `map(f, list) === [ f(x) for x in list ]`: Apply `f` element-wise to `list`.\n",
      "- `filter(f, list) === [ x for x in list if f(x) ]`: Filter `list` using `f`.\n",
      "- `flatMap(f, list) === [ f(x) for y in list for x in y ]`: Here `f` is a function that eats elements (of the type contained in list) and spits out lists, and `flatMap` first applies f element-wise to the elements of `list` and then _flattens_ or _concatenates_ the resulting lists.  It is sometimes also called `concatMap`.\n",
      "- `reduce(f, list[, initial])`: Here `f` is a function of _two_ variables, and folds over the list applying `f` to the \"accumulator\" and the next value in the list.  That is, it performs the following recursion\n",
      "\n",
      "$$    a_{-1} = \\mathrm{initial} $$\n",
      "$$    a_i = f(a_{i-1}, \\mathrm{list}_i) $$\n",
      "\n",
      "with the with the final answer being $a_{\\mathrm{len}(\\mathrm{list})-1}$.  (If initial is omitted, just start with $a_0 = \\mathrm{list}_0$.)  For instance,\n",
      ">           \n",
      "    reduce(lambda x,y: x+y, [1,2,3,4]) = ((1+2)+3)+4 = 10\n",
      "    \n",
      "    \n",
      "###Remark:\n",
      "This is where the name \"map reduce\" comes from.."
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "## PySpark notation\n",
      "Put the list first:\n",
      "- `list.map(f)`\n",
      "- `list.flatMap(f)`\n",
      "- `list.filter(f)`\n",
      "\n",
      "Composes better:\n",
      "- `list.map(f) \\\n",
      "    .map(g) \\\n",
      "    .filter(h) \\\n",
      "    .flatMap(j)`"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "## Nouns\n",
      "\n",
      "The two main abstractions in Spark are:\n",
      "\n",
      "1. Resilient distributed datasets (RDDs):\n",
      "  This is like a (smartly) distributed list, which is partitioned across the nodes of the cluster and can be operated on in parallel.  The operations take the form of the usual functional list operations as shown above.  There are two types of operations: Transformations and Actions.\n",
      "1. Shared variables used in parallel operations: When Spark runs a function in parallel across nodes, it ships a copy of each variable needed by that function to each node.  There are two types of shared variables:\n",
      " - Broadcast variables: which are used to store a value in memory across all nodes (i.e., their values are \"broadcast\" from the driver node)\n",
      " - Accumulator variables: these are variables which are only added to across nodes, for implementing counters and sums.\n",
      " \n",
      "## Verbs\n",
      " \n",
      "- [Transformations](http://spark.apache.org/docs/latest/programming-guide.html#transformations): This creates a new RDD from an old one, for instance `map` or `flatMap`.  Transformations in Spark are __lazy__, this means that they do not compute the answer right away -- only when it is needed by something else.  Instead they represent the \"idea\" of the transformation applied to the base data set (e.g., for each chaining).  In particular, this means that intermediate results to computations do not necessarily get created and stored -- if the output of your map is going straight into a reduce, Spark should be clever enough to realize this and not actually store the output of the map.  Another consequence of this design is that the results of transformations are, by default, recomputed each time they are needed -- to store them one must explicitly call `cache`.\n",
      "- [Actions](http://spark.apache.org/docs/latest/programming-guide.html#actions): These actually return a value as a result of a computation.  For instance, `reduce` is an action that combines all elements of an RDD using some function and then returns the final result.  (Note that `reduceByKey` actually returns an RDD.)\n"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "# Using the Spark interactive (Python) console\n",
      "For the interactive Spark console (Python version) run\n",
      "\n",
      ">      \n",
      "    cd ~/local-spark/spark-1.0.0-bin-hadoop2/\n",
      "    IPYTHON=1 bin/pyspark\n",
      "    \n",
      "to get an interactive (ipython) shell Spark, pre-loaded with an `sc` SparkContext variable."
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "# Using Spark from Python code\n",
      "Alternatively, we can use Spark from ordinary Python code -- including IPython Notebook.  The Spark installer script should have added a few lines to your `~/.bashrc` file that sets this up.  In order for the code below to work in ipython notebook you may need to launch a new shell and restart ipython notebook from that shell.\n",
      "\n",
      "One disadvantage relative to the interactive console is that you won't see the progress report / execution info.  But you can see that sort of info by connecting to\n",
      ">            \n",
      "    http://[your ip]:4040\n",
      " "
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "# Example 1: Computation (pi)\n",
      "Our zeroeth example shows distributing a purely numerical, stateless, computation over Spark: computing $\\pi$ by throwing darts at a square and counting how many land in the circle.\n",
      "\n",
      "The `parallelize` command takes a local collection (e.g., list) and makes an RDD out of it -- there are two parameters: the local collection, and the parameter controlling how many pieces to chop it into for the purposes of parallelizing later.  (If the collection is very large you should explicitly pass in this parameter, otherwise you will run out of memory in the workers.)\n",
      "\n",
      "First, in `numpy` and `matplotlib`:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "import numpy as np\n",
      "import matplotlib.pylab as plt\n",
      "\n",
      "SIZE = 10**6\n",
      "PLOT_N = 1000\n",
      "\n",
      "x = np.random.uniform(size=(SIZE, 2))\n",
      "in_circle = np.linalg.norm(x, axis=1) < 1.\n",
      "out_of_circle = np.linalg.norm(x, axis=1) > 1.\n",
      "\n",
      "fig = plt.figure()\n",
      "ax = fig.add_subplot(111, aspect='equal')\n",
      "plt.plot(x[in_circle,::][::(SIZE/PLOT_N), 0], x[in_circle,::][::(SIZE/PLOT_N), 1], '.')\n",
      "plt.plot(x[out_of_circle,::][::(SIZE/PLOT_N), 0], x[out_of_circle,::][::(SIZE/PLOT_N), 1], 'r.')\n",
      "\n",
      "print 4. * in_circle.sum().astype(float) / SIZE"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Now in `pyspark`:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "import random\n",
      "\n",
      "CHUNK_SIZE=10**5\n",
      "CHUNKS = 100\n",
      "\n",
      "samples = CHUNK_SIZE * CHUNKS\n",
      "count_needles = sc.parallelize(xrange(samples), CHUNKS) \\\n",
      "    .map(lambda _: (random.random(), random.random())) \\\n",
      "    .filter(lambda (x,y): x*x+y*y < 1) \\\n",
      "    .count()\n",
      "    \n",
      "print 4.0 * count_needles / (samples)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "### Exercises:\n",
      "1. Modify the above code to approximate the center of mass of a (constant density) quarter-circle of radius $1$.\n",
      "2. Play around (at least a little) with the CHUNK_SIZE and CHUNKS parameters to see what does and doesn't work.  Can you explain what's happening when it doesn't work?\n"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "# Example 2: List processing (word count)\n",
      "\n",
      "Our first example is practice for basic \"functional list processing\": word count.  This is the second example at http://spark.apache.org/examples.html.  Try running the following code:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "from os.path import expanduser\n",
      "base = expanduser(\"~/datacourse/projects/spark-example/\")\n",
      "lines = sc.textFile(base + \"data/pg/pg5000.txt\")"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "counts = lines.flatMap(lambda line: line.split()) \\\n",
      "          .map(lambda word: (word, 1)) \\\n",
      "          .reduceByKey(lambda x,y: x+y)\n",
      "\n",
      "# The first 10 words in some order        \n",
      "print counts.take(10)\n",
      "\n",
      "# The top 10 words by count\n",
      "print counts.takeOrdered(10, key = lambda x: -x[1])"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "### Exercises:\n",
      "1. Modify the above to be case insensitive.\n",
      "1. Modify the above to return the count of the top 15 words that begin with the letter 'a'.\n",
      "1. Write a function to return the sum of the lengths of all words.\n",
      "1. Write a function to return the sum of the lengths of all words that begin with the letter 'a'.\n",
      "1. Write a function to return a list of pairs like ('a', count of words that start with the letter a) for all possible first letters (lower-cased)."
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "## Example 3: Aggregating and dis-aggregating (reversing a graph)\n",
      "\n",
      "The file `$HOME/datacourse/projects/spark-example/data/raw/links-simple-sorted.txt` describes a simple graph (the Wikipedia page link graph). Each row is a line like\n",
      ">           \n",
      "    23:1 15 78\n",
      "    \n",
      "telling us that page 23 links to pages 1, 15, and 78 (i.e., representing three edges 23->1, 23->15, 23->78). \n",
      "\n",
      "We want to reverse this association, and output a file whose lines represent *incoming* links.  That is, the above would be reflected by _parts_ of lines like:\n",
      ">          \n",
      "    1: .. 23 ...\n",
      "    15: ... 23 ...\n",
      "    78: ... 23 ...\n",
      "    \n",
      "#### Warning:\n",
      "This is actually a moderately sized problem.  In lecture I'll be running it on a machine with 20 virtual CPUs.  On your local machine or DO instance this might take a while!"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "!rm -rf ~/datacourse/projects/spark-example/data/links-reversed.txt"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "from os.path import expanduser\n",
      "base = expanduser(\"~/datacourse/projects/spark-example/\")"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "# N.B. \n",
      "# Spark _will_ automatically decompress gz files (not zip, though) -- but it will not split them up.\n",
      "# This is why we don't store this file on disk in compressed form.\n",
      "\n",
      "for x in sc.textFile(base + \"data/raw/links-simple-sorted.txt\").take(10):\n",
      "    print x"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "def sp_link(s):\n",
      "    s, ts = s.split(\":\")\n",
      "    s = int(s)\n",
      "    ts = map(int, ts.strip().split(\" \"))\n",
      "    return (s,ts)\n",
      "    \n",
      "    \n",
      "def reverse_edges(edges_out):\n",
      "    all_edges = edges_out.flatMap( lambda (s, ts): [(s,t) for t in ts])\n",
      "    edges_in = ( all_edges.map( lambda (s,t) : (t,s) )\n",
      "                          .groupByKey(numPartitions=20)\n",
      "                          )\n",
      "    edges_in_text = edges_in.map( lambda (t,ss): str(t)+\":\"+\" \".join(map(str, ss)) )\n",
      "    return edges_in_text\n",
      "\n",
      "# We set a high 'minPartitions' so that each partition doesn't need too mm\n",
      "edges_out = ( sc.textFile( base + \"data/raw/links-simple-sorted.txt\", minPartitions=100 )\n",
      "                .map(sp_link)\n",
      "                )\n",
      "\n",
      "# Write it to disk (uncomment -- but slow!)\n",
      "# reverse_edges( edges_out ).saveAsTextFile(base + \"data/links-reversed.txt\")"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "###Exercises:\n",
      "1. The nodes are numbered 1 through 5716808.  To run this on your DO instances in finite time, you might want to first sub-sample the graph (to 5% of the nodes).  Write code to do this by following this outline:\n",
      "    - Initialize a random Python set `in_sample` that stores whether a given node is in the sample.\n",
      "    - Modify `sp_link` to also filter entries to be in in_sample (hint: you may want to use `flatMap` intead of `map`)\n",
      "    - Cache the results, and use them going forward\n",
      "2. Write Spark code to print the top 10 nodes by in-degree.  Same for by out-degree.\n",
      "3. Find the Spark PageRank example (`examples/.../python/pagerank.py` in the spark folder).  Run PageRank on this graph (or a sub-sampled bit of the graph) to get recommended articles.\n",
      "4. Make the numPartitions and minPartitions values parameters, and then run the code in pyspark instead of ipython notebook.   Experiment with modifying these values and note when you do/don't get memory errors, how it performs when you don't, etc.  (You shhuld, for instance, seee memory errors when you decrease one/both of them -- this is simply because each chunk will have intermediate computations that are too large for a worker's memory!)"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "## Solution to 1 above.\n",
      "import random\n",
      "from os.path import expanduser\n",
      "base = expanduser(\"~/datacourse/projects/spark-example/\")\n",
      "\n",
      "SAMPLE_RATE = 0.05\n",
      "in_sample = { i for i in xrange(1,1 + 5716808) if random.random() < SAMPLE_RATE }\n",
      "\n",
      "def sp_link_filter(s):\n",
      "    s, ts = s.split(\":\")    \n",
      "    s = int(s)\n",
      "    if s not in in_sample:\n",
      "        return []\n",
      "    ts = [t for t in map(int, ts.strip().split()) if t in in_sample]\n",
      "    return [(s,ts)]\n",
      "\n",
      "edges_out_sample = (  sc.textFile( base + \"data/raw/links-simple-sorted.txt\", minPartitions=100 )\n",
      "                        .flatMap(sp_link_filter)\n",
      "                        )\n",
      "edges_out_sample.cache()"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print reverse_edges( edges_out_sample ).take(10)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "reverse_edges( edges_out_sample ).saveAsTextFile(base + \"data/links-reversed.txt\")"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "%ls ~/datacourse/projects/spark-example/data/links-reversed.txt\n",
      "!head -10 ~/datacourse/projects/spark-example/data/links-reversed.txt/part-00000"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "## Example 3: Joins\n",
      "\n",
      "The [Hadoop and Java_Mapreduce](Hadoop_and_Java_Mapreduce.ipynb) discuss how to implement a `join` in MR using \"sum types\" -- this is a data type that can consist of multiple \"types\" of data.  For instance, the output of an intermediate map step might have rows like\n",
      ">          \n",
      "    (\"Business\", (17, \"Joe's Barbershop\"))\n",
      "    (\"Business\", (23, \"Dave's Bistro\"))\n",
      "    (\"Person\", (17, \"Joe Schmoe\"))\n",
      "    (\"Person\", (23, \"David K. Noname\"))\n",
      "\n",
      "Fortunately for users of Spark, Spark includes a built-in `join` implementation.  From the documentation:\n",
      "\n",
      ">         \n",
      "join(otherDataset, [numTasks]):\t When called on datasets of type (K, V) and (K, W), returns a dataset of (K, (V, W)) pairs with all pairs of elements for each key. Outer joins are also supported through leftOuterJoin and rightOuterJoin."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "edges_sample_all = edges_out_sample.flatMap( lambda (s,ts): [(s,t) for t in ts] )\n",
      "edges_sample_all_flip = edges_sample_all.map( lambda (s,t): (t,s) )\n",
      "\n",
      "# Paths of length 2 via join:\n",
      "# Joining {(target,source)} with {(source,target)} gives \"paths\" { (middle, (first, last)) }\n",
      "print edges_sample_all_flip.join(edges_sample_all).take(10)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "## Example 4: Iterative algorithms (training a logistic regression)\n",
      "\n",
      "This is the last example from http://spark.apache.org/examples.html (.. mixed with an MLLib example)."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "import numpy as np\n",
      "N = 10**6\n",
      "fraction_positive = 0.5\n",
      "\n",
      "def y(x):\n",
      "    return 1 if x < fraction_positive else 0\n",
      "\n",
      "\"\"\"\n",
      "Generate Sample Data:\n",
      "\n",
      "We're going to try to learn the rule that\n",
      "y(x) = 1 if x<fraction_positive, 0 otherwise\n",
      "\n",
      "Our training X's will go evenly from 0 to 1\n",
      "\"\"\"\n",
      "def generate_sample():\n",
      "    sample_X = np.arange(0, 1, 1.0/N)\n",
      "    np.random.shuffle( sample_X) # In-place shuffle!\n",
      "    sample_Y = map(y, sample_X)\n",
      "    return (sample_X, sample_Y)\n",
      "\n",
      "(sample_X, sample_Y) = generate_sample()"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "## Using MLLib and it's data structures.  This is fairly quick.\n",
      "from pyspark.mllib.classification import LogisticRegressionWithSGD\n",
      "from pyspark.mllib.regression import LabeledPoint\n",
      "\n",
      "points = ( sc.parallelize( zip(sample_X, sample_Y), 100)\n",
      "             .map(lambda (x,y): LabeledPoint(y, [1, x]))\n",
      "             .cache() )\n",
      "model = LogisticRegressionWithSGD.train(points)\n",
      "\n",
      "# Evaluating the model on training data\n",
      "labelsAndPreds = points.map(lambda p: (p.label, model.predict(p.features)))\n",
      "trainErr = labelsAndPreds.filter(lambda (v, p): v != p).count() / float(points.count())\n",
      "print \"Accuracy on training set: %s\" % (1 - trainErr)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "## By hand.  This is the example code taken from the Spark Examples on the website.\n",
      "#  This is much slower than the above code, so I'm not going to even run it (or extract predictions, or test it..)\n",
      "\n",
      "def logistic_by_hand():\n",
      "    ITERATIONS=20\n",
      "    points = ( sc.parallelize( zip(sample_X, sample_Y), 20)\n",
      "                 .map(lambda (x,y): LabeledPoint(y, [1, x]))\n",
      "                 .cache() )\n",
      "    w = np.random.ranf(size = 2) # current separating plane\n",
      "    print \"Original random plane: %s\" % w\n",
      "    for i in xrange(ITERATIONS):\n",
      "        gradient = points.map(\n",
      "            lambda pt: (1 / (1 + np.exp(-pt.label*(w.dot(pt.features)))) - 1) * pt.label * pt.features\n",
      "        ).reduce(lambda a, b: a + b)\n",
      "        w -= gradient\n",
      "    print \"Final separating plane: %s\" % w\n",
      "\n",
      "# logistic_by_hand()"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "### Exercises\n",
      "1. Play with the `fraction_positive` parameter: What happens to the accuracy measure as `fraction_positive` gets below 0.30 or above 0.70? (You should be somewhat disappointed with the results!)  What do you think is happening, and can you improve on it?\n",
      "1. Play with the \"by hand\" version (.. after lowering N to say 10**4 or so): Figure out what it's actually doing and how to use it to get results.  How much slower than the MLLib version does it seem to be?"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "## Spark from IPython Notebook (dis-)continued:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "## Stop our spark context\n",
      "sc.stop()"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "*Copyright &copy; 2015 The Data Incubator.  All rights reserved.*"
     ]
    }
   ],
   "metadata": {}
  }
 ]
}