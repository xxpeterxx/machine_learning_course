#!/bin/bash
set -e 

if [[ $1 == "--help" ]];
	then
	echo "Try one of these options"
	echo "./test_via_rsync -a  # test all"
	echo "./test_via_rsync lessons/sample.ipynb  # test lessons/sample.ipynb"
	echo "./test_via_rsync  # test all edited ipynb"
  echo "see test_ipynb.sh for more documentation"
	exit 0
fi

if [[ ! -e .rsync_utils/rsync_command.txt ]]; then
  >&2 echo "Have you run setup_rsync.sh!?" 
  >&2 echo "Probably not, because you are missing the rsync_command.txt file that script generates" 
  exit 1
fi

. .rsync_utils/rsync_command.txt

if [[ ! -e .rsync_utils/hostname.txt ]]; then
  >&2 echo "Have you run setup_rsync.sh!?" 
  >&2 echo "You do not have a .rsync_utils/hostname.txt file. You'll need to add the hostname or ip of your digitalocean instance to proceed." 
  >&2 echo "It's a one-liner, simply create a file and add *only* the IP address (or hostname). Again, there should be only one line." 
  exit 1
fi

DO_HOSTNAME=$(head -n 1 .rsync_utils/hostname.txt)

rsync_to_digitalocean

ssh vagrant@$DO_HOSTNAME "cd datacourse; test/test_ipynb.sh $@" 
